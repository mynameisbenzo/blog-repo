from django.urls import path

from . import views

app_name = "portfolio"

urlpatterns = [
	# portfolio selection
	path('portfolio/', views.portfolio, name='portfolio'),
	
	# load experience entries
	path('portfolio/experience/', views.experience, name='experience'),
	
	# load project entries
	path('portfolio/projects/', views.projects, name='projects'),
]