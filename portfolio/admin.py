from django.contrib import admin

from .models import ExperienceEntry, ProjectEntry

admin.site.register(ExperienceEntry)
admin.site.register(ProjectEntry)