from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db import models
from django.utils import timezone

from multiprocessing import Process, Manager
from datetime import datetime
import re, requests

import time

from .models import ExperienceEntry, ProjectEntry
from .utils import time_elapsed

from blog.universal_utils import next_prev_gen

'''
	display portfolio page (deprecated - replaced with modal)
'''
def portfolio(request):
	if True:
		return HttpResponseRedirect(reverse('index'))
	return render(request, 'portfolio/portfolio.html')

'''
	display experiences page (deprecated - replaced with modal)
'''
def experience(request):
	if True:
		return HttpResponseRedirect(reverse('index'))
	experience_entries = ExperienceEntry.objects.order_by('-id')
	
	links = grabPlinks(experience_entries)
	
	earliest = ExperienceEntry.objects.order_by('started')[0]
	latest = ExperienceEntry.objects.order_by('-ended')[0]
	
	context = {
		'experience': experience_entries, 
		'years': time_elapsed(earliest.started, latest.ended),
		'test':links
	}
	return render(request, 'portfolio/experience.html', context)

'''
	display projects page will be retooled to display specific projects
'''	
def projects(request):
	start, finish = 0, 2
	projects = ProjectEntry.objects.order_by('-id')
	data = next_prev_gen(start, finish, finish-start, request.GET, projects)
	context = {'projects': data['segment'], 
		'test': grabPlinks(data['segment']),
		'current':data['start'], 
		'next':data['finish'],
		'numProj':len(projects)
	}
	return render(request, 'portfolio/projects.html', context)
	
def grabPlinks(projects):
	plinks = []
	for p in projects:
		plinks.append({p.id:p.link})
	return plinks