# Generated by Django 2.0.4 on 2018-06-12 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experienceentry',
            name='port_image',
        ),
        migrations.RemoveField(
            model_name='projectentry',
            name='port_image',
        ),
        migrations.AddField(
            model_name='projectentry',
            name='link',
            field=models.CharField(default='empty', max_length=200),
            preserve_default=False,
        ),
    ]
