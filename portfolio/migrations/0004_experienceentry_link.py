# Generated by Django 2.0.4 on 2018-06-12 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0003_auto_20180612_1554'),
    ]

    operations = [
        migrations.AddField(
            model_name='experienceentry',
            name='link',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
