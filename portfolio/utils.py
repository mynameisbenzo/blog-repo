from datetime import datetime
import math


def time_elapsed(start, end):
	total = end - start
	float_repr_total = total.days / 365
	int_repr_total = int(float_repr_total)
	if (float_repr_total - int_repr_total) > 0.8:
		exp = math.ceil(float_repr_total)
	elif (float_repr_total - int_repr_total) > 0.4:
		exp = int_repr_total + 0.5
	else:
		exp = int_repr_total
	return exp