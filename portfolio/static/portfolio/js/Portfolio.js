class Portfolio{
	constructor(){
		this.test = document.getElementById('test');
		this.experience = new Modal('exp');
		this.experience.goDirection('right');
		this.experience.getData('p');
	}
	animate(){
		this.experience.update();
		requestAnimationFrame(this.animate.bind(this));
	}
}
var p = new Portfolio();
p.animate();