from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
	#production
	url(r'wss/portfolio/experience/', consumers.ImgLinkConsumer),
	url(r'wss/portfolio/projects/', consumers.ImgLinkConsumer),
	#development
	url(r'ws/portfolio/experience/', consumers.ImgLinkConsumer),
	url(r'ws/portfolio/projects/', consumers.ImgLinkConsumer),
]