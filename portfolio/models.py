from django.db import models

class ExperienceEntry(models.Model):
	title = models.CharField(max_length=75)
	port_desc = models.TextField()
	started = models.DateField(blank=True)
	ended = models.DateField(blank=True)
	link = models.CharField(max_length=200, blank=True)
	
	class Meta:
		verbose_name_plural = "exp_entries"
	
	def __str__(self):
		return self.title + " : " + self.port_desc
		
class ProjectEntry(models.Model):
	title = models.CharField(max_length=75)
	port_desc = models.TextField()
	link = models.CharField(max_length=200, blank=True)
	img = models.CharField(max_length=200, blank=True, default='')
	
	class Meta:
		verbose_name_plural = "pro_entries"
	
	def __str__(self):
		return self.title + " : " + self.port_desc