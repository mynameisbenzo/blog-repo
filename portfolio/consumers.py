from channels.generic.websocket import AsyncWebsocketConsumer

import json, requests, re

class ImgLinkConsumer(AsyncWebsocketConsumer):
	async def connect(self):
		self.room_group_name = 'img_gather'
		
		# join
		await self.channel_layer.group_add(
			self.room_group_name,
			self.channel_name
		)
		
		await self.accept()
		
	async def disconnect(self, close_code):
		# leave
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
		)
		
	# receive message from socket
	async def receive(self, text_data):
		text_data_json = json.loads(text_data)
		message = text_data_json['img_gather_message']
	
		message['links'] = self.exchange_links(message['links'])
		# send message to room group
		await self.channel_layer.group_send(
			self.room_group_name,
			{
				'type': 'img_gather_message',
				'requested_links': message
			}
		)
		
	# receive message from room group
	async def img_gather_message(self, event):
		message = event['requested_links']
		# send message to websocket
		await self.send(text_data=json.dumps({
			'img_gather_message': message
		}))
		
	def exchange_links(self, url_group):
		for obj in url_group:
			for k,v in obj.items():
				if v != "":
					r = requests.get(v)
					html = r.text
					pat = re.compile(r'<img [^>]*src="([^"]+)')
					img = pat.findall(html)
					if img == []:
						obj[k] = ""
					else:
						obj[k] = img[0]
				else:
					obj[k] = ""
		return url_group