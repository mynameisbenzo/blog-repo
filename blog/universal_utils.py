# utilities that can be useful for any app

def next_prev_gen(p, n, limit, data, segmentMe):
	if data == {}:
		return {'segment':segmentMe[p:n], 'start':p, 'finish':n}
	elif 'next' in data:
		p = int(data['next'])
		n = p + limit
		if n > len(segmentMe):
			n = len(segmentMe)
	elif 'prev' in data:
		n = int(data['prev'])
		p = n - limit
		if p < 0:
			p = 0
	segmentMe = segmentMe[p:n]
	if n - p != limit:
		n = -1
	return  {'segment':segmentMe, 'start':p, 'finish':n}