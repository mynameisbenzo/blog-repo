"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

import blogs.urls as blogs
import users.urls as users
import portfolio.urls as portfolio
import blogposts_api.urls as blogposts_api
import stock_ticker.urls as stock_ticker
import youtube_playlist.urls as yt

from tastypie.api import Api
import blogposts_api.resources as r

v1_api = Api(api_name='v1')
v1_api.register(r.BlogPostResource())
v1_api.register(r.PhrasesResource())
v1_api.register(r.PictureInfoResources())
v1_api.register(r.ProjectResource())
v1_api.register(r.ExperienceResource())

urlpatterns = [
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += blogs.urlpatterns
urlpatterns += users.urlpatterns
urlpatterns += portfolio.urlpatterns
urlpatterns += blogposts_api.urlpatterns
urlpatterns += stock_ticker.urlpatterns
urlpatterns += yt.urlpatterns

urlpatterns += [
	path('api/', include(v1_api.urls)),
]