import os
import django
from channels.routing import get_default_application
from channels.staticfiles import StaticFilesWrapper

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'blog.settings')
django.setup()
application = StaticFilesWrapper(get_default_application())