from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import stock_ticker.routing
import portfolio.routing
import blogs.routing

application = ProtocolTypeRouter({
	'websocket': AuthMiddlewareStack(
		URLRouter(
			stock_ticker.routing.websocket_urlpatterns + 
			portfolio.routing.websocket_urlpatterns + 
			blogs.routing.websocket_urlpatterns
		),
	),
})