from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
from django.urls import reverse
from django.template import loader

import requests, json, re

from .forms import YTURLForm
from .utils import *

def yt_auth(request):
	temp_out = True
	if temp_out:
		return HttpResponse(loader.render_to_string('503.html'), status=503)
	'''
		section will need more work.  yt_auth works fine but the amount of 
		calls should be reduced.  
	'''
	context = {}
	example = None
	if request.method == "POST":
		example = requests.get('https://accounts.google.com/o/oauth2/v2/auth?'+
			'scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fyoutube.readonly&'+
			'access_type=offline&'+
			'include_granted_scopes=true&'+
			'state=state_parameter_passthrough_value&'+
			'redirect_uri=http%3A%2F%2Flocalhost:8000%2Fyt%2Fauth&'+
			'response_type=code&'+
			'client_id=' + settings.YT_C_ID)
		context['renderThis'] = example.text	
		return render(request, 'youtube_playlist/yt_auth.html', context)
	else:
		auth = request.GET['code']
		data = {
			'code': auth,
			'client_id': settings.YT_C_ID,
			'client_secret': settings.YT_C_S,
			'redirect_uri': 'http://localhost:8000/yt/auth',
			'grant_type': 'authorization_code'
		}
		r = requests.post('https://www.googleapis.com/oauth2/v4/token', data=data)
		request.session['credentials'] = r.text
		return HttpResponseRedirect(reverse('yt_shuffle'))

def shuffle(request):
	temp_out = True
	# if temp_out:
		# return HttpResponse(loader.render_to_string('503.html'), status=503)
	error = ''
	lists = []
	credentials = None
	ytform = YTURLForm()
	""" section will be used for youtube auth later """
	# if request.session.has_key('credentials'):
		# credentials = json.loads(request.session['credentials'])
	# if credentials != None and credentials['expires_in'] > 0:
		# r = requests.get('https://www.googleapis.com/youtube/v3/channels?access_token=' +
			# credentials['access_token'] +'&part=snippet%2cstatistics&mine=true'
		# )
		# my_j = json.loads(r.text)
		# try:
			# lists += playlistInfo(my_j['items'][0]['id'], True)
		# except Exception as e:
			# error = "Invalid login credentials for Youtube login."
	if request.method == 'GET':
		if 'yt_URL' in request.GET:
			my_re = re.compile(r'list[\s=](.*)')
			pid = re.search(my_re, request.GET['yt_URL']).group(1)
			if not copyCheck(pid, lists):
				try:
					lists += playlistInfo(pid)
				except Exception as e:
					error = 'Playlist error.'
		if len(lists) > 0:
			for x in lists:
				x['videos'] = retrievePlaylistItems(x['id'])
	context = {'ytform':ytform,'error':error, 'lists':lists}
	return render(request, 'youtube_playlist/yt_url_submit.html', context)