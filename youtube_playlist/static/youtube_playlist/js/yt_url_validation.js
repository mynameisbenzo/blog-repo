function createMessage(msg){
	var d = document.createElement('DIV');
	d.setAttribute("class", "alert alert-danger");
	d.setAttribute("role", "alert");
	d.style.cssText = 'color: black !important;';
	d.innerHTML = msg;
	return d;
}

//https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url
function ValidURL(str, inMsg) {
	console.log(str);
	var pattern = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/; // fragment locater
	if(!pattern.test(str)) {
		inMsg.innerHTML = '';
		inMsg.appendChild(createMessage("That's not a valid URL!"));
		return false;
	} else {
		inMsg.innerHTML = '';
		return true;
	}
}

// check if it's youtube url 
function YTLISTcheck(url, inMsg){
	var base = url.split('/');
	// this should be a valid url.
	var check = ['youtube', 'list='];
	for(var i = 0; i < base.length; i++){
		if(base[i].includes(check[0])){
			check.shift();
			if(check.length == 0) {
				inMsg.innerHTML = '';
				return true;
			}
		}
	}
	inMsg.innerHTML = '';
	inMsg.appendChild(createMessage("That's not a Youtube Playlist URL!"));
	return false;
}

// enable send to back end to retrieve songs
function activateSend(s, c, i){
	if(ValidURL(s, i) && YTLISTcheck(s, i)){
		c.disabled = false;
	}else{
		c.disabled = true;
	}
}

// will send youtube songs if given the correct info
var send = document.getElementById('validate');
send.disabled = true;

// will be used to check youtube url
var input = document.getElementById('id_yt_URL');
input.addEventListener('keyup', function(e){
	activateSend(input.value, send, invalidMsgDisp);
});

// onpaste has issues with right click paste and not getting actual input.value
// oninput works better
input.oninput = function(e){
	activateSend(e.target.value, send, invalidMsgDisp);
}

// will display message if url is invalid
var invalidMsgDisp = document.getElementById('loadInvalidMsg');