from django.conf import settings

import requests, json

# check for duplicate playlist ids
def copyCheck(pid, vid):
	for v in vid:
		if v['id'] == pid:
			return True
	return False

# get playlist json with relevant information (id, image url, and title)
def playlistInfo(playlist_id, channel=False):
	callThis = ('https://www.googleapis.com/youtube/v3/playlists' +
		'?part=snippet%2CcontentDetails')
	if channel:
		callThis += ('&channelId=' + playlist_id +
				'&maxResults=25&')
	else:
		callThis += '&maxResults=50'+'&id=' + playlist_id 
	callThis += '&key=' + settings.YT_API
	r = requests.get(callThis)
	plist = []
	my_j = json.loads(r.text)
	for k in my_j['items']:
		list = {
			'id':k['id'],
			'img':k['snippet']['thumbnails']['high']['url'],
			'title':k['snippet']['localized']['title'],
		}
		plist.append(list)
	return plist
	
# get videos from a playlist with a given playlist id	
def retrievePlaylistItems(id):
	videos = []
	r = requests.get('https://www.googleapis.com/youtube/v3/playlistItems' +
		'?part=snippet%2CcontentDetails' +
		'&maxResults=50'+
		'&playlistId=' + id + 
		'&key=' + settings.YT_API)
	my_j = json.loads(r.text)
	for k in my_j['items']:
		video = {
			'id': k['contentDetails']['videoId'],
			'img': k['snippet']['thumbnails']['high']['url'],
			'title': k['snippet']['title'],
		}
		videos.append(video)
	return videos