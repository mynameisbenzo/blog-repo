# chat/urls.py
from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    url(r'portfolio/projects/shuffle', views.shuffle, name='yt_shuffle'),
	url(r'yt/auth', views.yt_auth, name='yt_auth'),
]