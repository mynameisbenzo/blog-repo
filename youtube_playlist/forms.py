from django import forms

import re

class YTURLForm(forms.Form):
    yt_URL = forms.CharField(label='Link URL', max_length=300)