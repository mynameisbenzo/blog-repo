from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db import models
from django.utils import timezone
from django.core.mail import EmailMessage

import datetime, os, json, requests

from .models import BlogPost
from portfolio.models import ProjectEntry
from .forms import BlogPostForm
from .utils import init_keys
from blog.universal_utils import next_prev_gen

'''
	project for anazay
'''
@login_required
def dog(request):
	return render(request, 'blogs/doggo.html', {})

'''
	project for arlette
'''
def dontpushme(request):
	return render(request, 'blogs/secret.html', {})

'''
	canvas page
'''
def canvas(request):
	return render(request, 'blogs/home.html', {})

	#################################################
	#	From here down is actual website stuff		#
	#	show blogs, edit them, make new ones etc	#
	#################################################

'''
	canvas page
'''
def birthday(request):
	return render(request, 'blogs/birthday.html', {})

	#################################################
	#	From here down is actual website stuff		#
	#	show blogs, edit them, make new ones etc	#
	#################################################

'''
	home page
'''
def home(request):
	init_keys()
	if BlogPost.objects.exists():
		post = BlogPost.objects.order_by('-date_added')[0]
	else:
		post = None
	if ProjectEntry.objects.exists():
		project = ProjectEntry.objects.order_by('-id')[:2]
	else:
		project = None
	context = {
		'post': post,
		'project':project,
	}
	return render(request, 'blogs/index.html', context)

'''
	blogs page
'''
def blogs(request):
	start, finish = 0, 5
	blogposts = BlogPost.objects.order_by('-date_added')
	data = next_prev_gen(start, finish,
		finish-start, request.GET, blogposts)
	context = {
		'blogposts': data['segment'],
		'current':data['start'],
		'next':data['finish']
	}
	return render(request, 'blogs/blog.html', context)

'''
	create new post
'''
@login_required
def new_post(request):
	if request.method != 'POST':
		post = BlogPostForm()
	else:
		post = BlogPostForm(request.POST)
		if post.is_valid():
			new_post = post.save(commit=False)
			new_post.owner = request.user
			new_post.save()
			return HttpResponseRedirect(reverse('index'))

	context = {'post':post}
	return render(request, 'blogs/new_post.html', context)

'''
	edit previous post
'''
@login_required
def edit_post(request, post_id):
	post = get_object_or_404(BlogPost, id=post_id)

	# check owner against current user
	if post.owner != request.user:
		raise Http404

	if request.method != 'POST':
		blogpost = BlogPostForm(instance=post)
	else:
		# update time of post to show recent activity on post
		try:
			BlogPost.objects.filter(id=post_id).update(
				date_added=datetime.datetime.now(tz=timezone.utc)
			)
			post = BlogPost.objects.get(id=post_id)
		except Exception as e:
			print(e)

		blogpost = BlogPostForm(instance=post, data=request.POST)
		if blogpost.is_valid():
			blogpost.save()
			return HttpResponseRedirect(reverse('index'))

	context = {'post':post, 'blogpost':blogpost}
	return render(request, 'blogs/edit_post.html', context)
