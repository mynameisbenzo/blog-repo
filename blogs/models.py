from django.db import models
from django.contrib.auth.models import User

# for use in blogpost creation/editing
class BlogPost(models.Model):
	title = models.CharField(max_length=75)
	text = models.TextField()
	date_added = models.DateTimeField(auto_now_add=True)
	owner = models.ForeignKey(
		User,
		on_delete=models.CASCADE
	)
	
	class Meta:
		verbose_name_plural = 'blogposts'
	
	def __str__(self):
		return self.title + " " + self.text + " " + self.owner.username
		
# for use when user wants to create a login
class NewUserRequest(models.Model):
	email = models.CharField(max_length=100)
	key = models.CharField(max_length=500)
	user_exists = models.BooleanField()
	
	class Meta:
		verbose_name_plural = 'NewUserRequests'
		
	def __str__(self):
		return self.email + " : " + self.key
		
# for use in home page picture retrieval
class PictureInfo(models.Model):
	link = models.CharField(max_length=200)
	location = models.CharField(max_length=100)
	
	class Meta:
		verbose_name_plural = 'pictures'
	
	def __str__(self):
		return self.location
		
# will keep track of amount of times a ticker is called
class TickerCalls(models.Model):
	ticker = models.CharField(max_length=20)
	amt = models.IntegerField()
	
	class Meta:
		verbose_name_plural = "tickercalls"
	
	def __str__(self):
		return self.ticker
		
# keep track of high scores
class HighScores(models.Model):
	game = models.CharField(max_length=20)
	name = models.CharField(max_length=5)
	score = models.IntegerField()
	
	class Meta:
		verbose_name_plural = 'highscores'
	
	def __str__(self):
		return self.game + " " + self.name #+ " " + self.score