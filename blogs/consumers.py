from django.conf import settings
from channels.generic.websocket import AsyncWebsocketConsumer

import json, requests

class EmailConsumer(AsyncWebsocketConsumer):
	async def connect(self):
		self.room_group_name = 'email'
		
		# join
		await self.channel_layer.group_add(
			self.room_group_name,
			self.channel_name
		)
		
		await self.accept()
		
	async def disconnect(self, close_code):
		# leave
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
		)
		
	# receive message from socket
	async def receive(self, text_data):
		email_data_json = json.loads(text_data)
		message = email_data_json['send_email']
		headers = {
			"Authorization":'Bearer ' + settings.SG_API,
			'Content-Type':'application/json',
		}
		data = {
			'personalizations': [{
				'to': [{'email':'lorhernandez@csumb.edu'}]
			}], 
			'from':{'email':'lorhernandez@csumb.edu'},
			'subject':'sendgrid testing',
			'content':
			[{
					'type':'text/plain',
					'value':('Email: ' + '@'.join(message['email'])+
							' Name: ' + message['name'] + 
							' Message: ' + message['msg'])
			}]
		}
		
		r = requests.post('https://api.sendgrid.com/v3/mail/send',headers=headers,data=json.dumps(data))
		
		# send message to room group
		await self.channel_layer.group_send(
			self.room_group_name,
			{
				'type': 'send_email',
				'send_email': 'sent'
			}
		)
		
	# receive message from room group
	async def send_email(self, event):
		message = event['send_email']
		# send message to websocket
		await self.send(text_data=json.dumps({
			'send_email': message
		}))