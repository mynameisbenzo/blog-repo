function fadeClassIn(next_step, fadeThis){
	var e;
	for(var x = 0; x < $(fadeThis).length+1; x++){
		e = fadeThis + ':nth-child('+ x +')';
		$(e).animate({
			opacity: next_step
		},{
			duration:100,
		});
	}
}
function readjustCanvasButton(){
	var readjust = document.getElementById('navStyleCanvas');
	readjust.style.top = (window.innerHeight - readjust.clientHeight - 5) + "px";
}
readjustCanvasButton();

var url = window.location;
var base = url.pathname.split('/');
if(base[3] != 'shuffle'){
	$('.panel').css('opacity', 0);
}
var t = 100, step = 0.25;
for(var y = 0; step <= 1; y++){
	setTimeout(fadeClassIn.bind(null, step, '.panel'), t);
	t+= y;
	step += 0.25;
}
var tout;
$('#hoverExpand').hover(function(){
	if(!$('#nav-dropdown').is(':visible')) $('#nav-dropdown').slideDown('slow');
});
$('#hoverExpand').mouseleave(function(e){
	tout = setTimeout(function(){
		$('#nav-dropdown').slideUp('slow');
	}, 250);
});
//prevents blank out of page on click when on mobile
//thought is that on click it's normally supposed to expand, but
//since i want to to it on hover, it was causing problems
$('#hoverExpand').click(function(e){
	e.stopPropagation();
	e.preventDefault();
});
$('#nav-dropdown').mouseenter(function(){
	clearTimeout(tout);
});
$('#nav-dropdown').mouseleave(function(){
	$('#nav-dropdown').slideUp('slow');
});

window.onresize = readjustCanvasButton;