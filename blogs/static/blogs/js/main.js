var b = null, td = null, ti = null, tl = null;
window.onload = function(){
	b = new Background(document.getElementById('draw-here'));
	td = new TextDisplay(document.getElementById('display-here'));
	ti = new TextInput(document.getElementById('write-here'));
	needsReloading.push(ti);
	
	ti.sendTo(td, 'send-cmd');
	ti.sendTo(td, 'send-text');
	ti.sendTo(td, 'TI-sending');
	ti.sendTo(b, 'bg-change');
	ti.assignInput(document.getElementById('canvasCursor'));
	
	b.canvas.addEventListener('mousemove', function(e){
		// b.trail(e, b);
	});
	var message = "Hello there! My name is Lorenzo Hernandez III! I'm a recent graduate of CSU, Monterey Bay with a Bachelor's in Computer Science. Take a look at my projects and experience! Type 'help' to bring up commands."
	var pieces = Canvas.createPieces(td, message);
	td.canvas.addEventListener('click', function(){
		td.emptyMessages()
		td.loadMessage(pieces);
		pieces = [];
		td.writeMessage();
	});
	td.canvas.addEventListener('wl-done', function(e){
		td.writeMessage();
		if(e.detail.time != null) setTimeout(td.emptyMessages(), e.detail.time * 2);
	});
	ti.canvas.addEventListener('click', function(e){
		ti.inputElement.focus();
		ti.clearText();
	});
};
