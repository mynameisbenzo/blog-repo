// used to display messages
class TextDisplay extends Canvas{
	constructor(element){
		super(element);

		this.canvas.style.height = '30%';
		this.canvas.style.width = '80%';

		//shift to
		this.leftShift = "60%";
		this.rightShift = "80%";

		// 1 * 40 = 40, 0.75 * 40 = 30, 0.5 * 40 = 20, etc.
		// Note: less than 0.33 looks pretty gross.
		this.t_multiplier = 0.5;
		this.ctx.font = "" + (40 * this.t_multiplier) + "px VT323";
		this.canvas.style.backgroundColor = 'rgb(255, 255, 255)';
		this.ctx.fillStyle = 'black';
		this.ctx.textAlign = 'center';

		//events
		//writing is done
		this.writeLineFinish = document.createEvent('Event');
		this.writeLineFinish.initEvent('wl-done', true, true);

		//queued messages is empty
		this.qMessagesEmpty = document.createEvent('Event');
		this.qMessagesEmpty.initEvent('qm-empty', true, true);

		//clear buffer after x amount of time
		this.writeLineFinish = new CustomEvent('wl-done', {
			detail:{
				time: null,
			}
		});

		// Note: the following can be used for a text element class
		//newline manager in text creation
		//will hold dictionaries: key = y position, value = stored text, and
		//opacity = visibility of text line
		this.newLineState = [];
		// message to be printed.  messages to be treated as a queue.  fifo
		this.queued_messages = [];
		this.qLength = 0;

		// used to shift canvas up and down when keyboard activates
		this.upShift = "-10%", this.downShift = "20%";

		// load opening message
		this.loadMessage(["Click me","or", "type 'help' below."]);
		this.writeMessage();

		this._help = "Type 'help' to dismiss -->";

		// additional states
		this.states['helpTriggered'] = false;
	}
	/*
		Methods used to draw text
	*/
	drawCurrentAndPrev(o,str,b){
		o.drawManaged(o,30);
		o.ctx.fillText(str, o.centerx/4 + b, o.centery+20);
	}
	nextLetter(str, t){
		var a = t, b = 30;
		setTimeout(this.drawCurrentAndPrev(this, str, b), t);
		t += a, b += b;
		setTimeout(this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height), t);
		t += a;
		setTimeout(this.drawCurrentAndPrev(this, str, b), t);
		t += a, b += b;
		setTimeout(this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height), t);
		t += a;
		setTimeout(this.drawCurrentAndPrev(this, str, b), t);
		t += a;
	}
	// Clear previous messages
	emptyMessages(){
		this.newLineState.length = 0;
	}
	// loads given message into queue.  Single string or array
	loadMessage(str){
		(Array.isArray(str)) ? Array.prototype.push.apply(this.queued_messages, str) : this.queued_messages.push(str);
		if(this.qLength == 0) this.qLength = this.queued_messages.length;
	}
	// writes message character by character
	writeMessage(){
		var str = "";
		if(!this.states.writing){
			str = this.queued_messages[0];
			this.queued_messages.shift();
		}
		if(this.states.writing || str == undefined){
			this.qLength = 0;
			this.canvas.dispatchEvent(this.qMessagesEmpty);
			this.newLineState.length = 0;
			return;
		}
		this.states.writing = true;
		var t = 10,a = t * 5;
		for(var c = 0;  c < str.length + 1; c++){
			setTimeout(this.nextLetter.bind(this, str.slice(0, c), t), t);
			t += a;
		}

		setTimeout(this.manageText.bind(this,str,30), t);
		t += a;
		setTimeout(this.resetAndSend.bind(this, t), t);
	}
	resetAndSend(t){
		this.states.writing = false;
		if(this.queued_messages.length == 0) this.writeLineFinish.detail.time = t;
		this.canvas.dispatchEvent(this.writeLineFinish);
		this.writeLineFinish.detail.time = null;
	}
	// instantly displays message
	instantMessage(arr){
		this.emptyMessages();
		var adjust = 20;
		if(typeof arr != 'string') adjust *= arr.length/2;
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.loadMessage(arr);
		while(this.queued_messages.length > 0){
			this.manageText(this.queued_messages[0], 20);
			this.queued_messages.shift();
		}
		this.drawManaged(this, 30, adjust,1);
		this.emptyMessages();
	}
	/*
		newline functions
	*/
	// sets new line and it's y position
	manageText(text, ypos){
		if(text == undefined) return;
		var o = (this.qLength*4.5)/100;
		this.newLineState.forEach(function(element){
			element.y += ypos;
			element.opacity -= o;
		});
		this.newLineState.push({str:text, y:0, opacity:1-o});
	}
	// draws previous line after typing out entire message
	// b effects x position of text
	drawManaged(obj,b,adjust=0, op=undefined){
		var centering = Canvas.createCenteringX(b);
		var opacity = undefined;
		this.newLineState.forEach(function(element){
			opacity = (op != undefined) ? op : element.opacity;
			obj.ctx.fillStyle = "rgba(0,0,0,"+opacity+")";
			obj.ctx.fillText(element.str, obj.centerx/4 + centering, obj.centery-element.y+adjust);
		});
		obj.ctx.fillStyle = "rgba(0,0,0,1)";
	}
	// instantly display information in display
	loadCmd(cmd){
		Canvas.getPhrase(this, cmd);
	}
	// overriding process phrase
	processPhrase(phrase){
		var message = phrase['text'].split('&');
		switch(phrase['type']){
			case 'tips':
				message.unshift("Tip: ");
				break;
			case 'about_me':
				message.unshift("About me: ");
				break;
			case 'random_fact':
				message.unshift("Fact: ");
				break;
			case 'hints':
				message.unshift("Hint: ");
				break;
			case 'stocks':
				// message.unshift("NYSE: ");
				message = ["NYSE:","No stock data at the moment."];
				break;
			default:
				console.log('Phrase not recognized.  How did this happen?');
				break;
		}
		this.instantMessage(message);
	}
}

// works as a input='text' field
class TextInput extends Canvas{
	constructor(element){
		super(element);
		this.canvas.style.width = "80%";
		this.canvas.style.height = "10%";

		this.inputText = "Hello!";

		//shift to
		this.leftShift = "60%";
		this.rightShift = "80%";

		// Note: range 1.5 to 2.0 looks best
		this.t_multiplier = 1.75;
		this.ctx.font = "" + (40 * this.t_multiplier) + "px 'VT323'";
		this.canvas.style.backgroundColor = 'rgba(0, 0, 0, 0)';
		this.ctx.fillStyle = 'green';
		this.ctx.textAlign = 'center';

		this.ctx.fillText(Canvas.createChunk(this, "_"), this.centerx, this.centery);
		this.ctx.fillText(Canvas.createChunkWithPrepend(this, this.inputText), this.centerx, this.centery - 20);

		/*
			custom events
		*/
		// send data with message to load
		this.sendTextToLoadMessage = new CustomEvent('send-text', {
			detail:{
				str: '',
			}
		});
		// send data with background change information
		this.sendBgChange = new CustomEvent('bg-change', {
			detail:{
				type: null,
				data: null,
			}
		});
		// send data with message to load
		this.tdCmd = new CustomEvent('send-cmd', {
			detail:{
				cmd:'',
			}
		});

		//// for keyboard stretch
		this.tlHelp = new Event('TI-sending');
		this.tlHelp.initEvent('TI-sending', true, true);


		// objects this instance communicates with
		// holds event and canvas element
		// key : value ==> event : canvas
		this.messageTo = {
			'send-cmd' : null,
			'TI-sending' : null,
			'send-text': null,
		};

		// event types
		this._eventTypes = ['TI-sending', 'send-cmd'];
		this._cmdTypes = ['help','about','random','hint', 'go_back'];
		this._passThis = ['tips','about_me','random_fact','hints']
	}
	// called when google font is successfully loaded for a rewrite
	loadText(){
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.t_multiplier = 1.75;
		this.ctx.font = "" + (40 * this.t_multiplier) + "px 'VT323'";
		this.canvas.style.backgroundColor = 'rgba(0, 0, 0, 0)';
		this.ctx.fillStyle = 'green';
		this.ctx.textAlign = 'center';
		this.ctx.fillText(Canvas.createChunk(this, "_"), this.centerx, this.centery);
		this.ctx.fillText(Canvas.createChunkWithPrepend(this, this.inputText), this.centerx, this.centery - 20);
	}
	// checks if valid input
	isValidInput(key){
		return key.length == 1 || key == "Backspace" || key == "Enter";
	}
	// sets up keyboard
	assignInput(element){
		var o = this;
		this.inputElement = element;
		this.inputElement.addEventListener("keydown", function(e){
			if(e.ctrlKey != true){
				if(e.key.length == 1) o.keyboardInput(e.key);
				switch(e.key){
					case 'Backspace':
						o.backspace();
						break;
					case 'Enter':
						o.submit();
						break;
					default:
						break;
				}
				e.stopPropagation();
				e.preventDefault();
			}
		});
		this.leftIncrement = 7.5;
		this.lMax = 83.5, this.lMin = 16;
		this.setCursor(this.inputText.length);
	}
	// input left should be set to 16, never go above 83.5
	setCursor(position){
		this.inputElement.style.left = this.lMin + (this.leftIncrement * position) + "%";
	}
	// clear text and redraw lines
	clearText(clearInput=true){
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.fillText(Canvas.createChunk(this, "_"), this.centerx, this.centery);
		if(clearInput) this.inputText = '';
		this.setCursor(this.inputText.length);
	}
	// used to grab input and place into canvas text
	keyboardInput(someChar){
		if(this.inputText.length > 9) return;
		this.inputText += someChar.toLowerCase();
		this.clearText(false);
		this.ctx.fillText(Canvas.createChunkWithPrepend(this, this.inputText), this.centerx, this.centery - 20);
	}
	backspace(){
		if(this.inputText.length < 1) return;
		this.inputText = this.inputText.slice(0, this.inputText.length -1);
		this.clearText(false);
		this.ctx.fillText(Canvas.createChunkWithPrepend(this, this.inputText), this.centerx, this.centery - 20);
	}
	submit(){
		var check = this.inputText.trim();
		if(this._cmdTypes.indexOf(check) != -1) {
			this.clearText();
			this.sendCmd(this._passThis[this._cmdTypes.indexOf(check)]);
			return;
		};
		check = this.inputText.split(":");
		if(check.length > 1){
			switch(check[0]){
				case 'p':
					this.sendTextToLoadMessage.detail.str = check[1];
					this.messageTo['send-text'].dispatchEvent(this.sendTextToLoadMessage);
					break;
				case 's':
					this.sendBgChange.detail.type = 'star_type';
					this.sendBgChange.detail.data = check[1];
					this.messageTo['bg-change'].dispatchEvent(this.sendBgChange);
					break;
				case 'c':
					this.sendBgChange.detail.type = 'color';
					this.sendBgChange.detail.data = check[1];
					this.messageTo['bg-change'].dispatchEvent(this.sendBgChange);
					break;
				case 'b':
					this.sendBgChange.detail.type = 'bg-color';
					this.sendBgChange.detail.data = check[1];
					this.messageTo['bg-change'].dispatchEvent(this.sendBgChange);
					break;
				default:
					this.sendTextToLoadMessage.detail.str = "Unrecognized command!";
					this.messageTo['send-text'].dispatchEvent(this.sendTextToLoadMessage);
					break;
			}
		}
		this.clearText();
	}
	sendCmd(s){
		this.messageTo['TI-sending'].dispatchEvent(this.tlHelp);
		this.tdCmd.detail.cmd = s;
		this.messageTo['send-cmd'].dispatchEvent(this.tdCmd);
	}
	// used to establish where to send text to
	sendTo(obj, type){
		this.messageTo[type] = obj.canvas;
		switch(type){
			case 'send-cmd':
				obj.canvas.addEventListener(type, function(e){
					obj.loadCmd(e.detail.cmd);
				});
				break;
			case 'TI-sending':
				obj.canvas.addEventListener(type, function(e){
					// obj.toggle(true);
					// obj.broadcastChange();
				});
				break;
			case 'send-text':
				obj.canvas.addEventListener(type, function(e){
					if(e.detail.str == "Unrecognized command!"){
						obj.loadMessage(e.detail.str);
						obj.writeMessage();
						return;
					}
					obj.loadMessage("You said: " + e.detail.str);
					obj.writeMessage();
				});
				break;
			case 'bg-change':
				obj.canvas.addEventListener(type, function(e){
					switch(e.detail.type){
						case 'color':
							obj.bgeffect.setColor(e.detail.data);
							break;
						case 'star_type':
							obj.bgeffect.setStar(e.detail.data);
							break;
						case 'bg-color':
							obj.bgeffect.bgChange(e.detail.data);
							break;
						default:
							break;
					}
				});
				break;
			default:
				console.log('Event type not recognized.');
				break;
		}
	}
	//toggle cursor position to accomodate toggle states
	cursorRecalc(){
		if(this.states.kbVisible){
			this.leftIncrement *= 0.75;
			this.lMax *= 0.75, this.lMin *= 0.9;
			this.setCursor(this.inputText.length);
		}else{
			this.leftIncrement = 7.5;
			this.lMax = 83.5, this.lMin = 16;
			this.setCursor(this.inputText.length);
		}
	}
}
// will be used to display a list of text
class TextList extends Canvas{
	constructor(element){
		super(element);
		this.canvas.style.width = "20%";
		this.canvas.style.height = "60%";
		this.canvas.style.left = "100%";

		//shift to
		this.leftShift = "80%";
		this.rightShift = "100%";

		this.listOfText = [];

		// Note: range 1.5 to 2.0 looks best
		this.t_multiplier = 2;
		this.ctx.font = "" + (15 * this.t_multiplier) + "px VT323";
		this.canvas.style.backgroundColor = 'rgb(255, 255, 255, 1)';
		this.ctx.textAlign = 'center';

		// toggle elements
		this.tElements = [];

		this.toggleEvent = new Event('keyboardShowing');
		this.toggleEvent.initEvent('keyboardShowing', true, true);

		// cmds
		this.displayCmd();
	}
	broadcastChange(){
		var e = this.toggleEvent;
		this.tElements.forEach(function(element){
			element.canvas.dispatchEvent(e);
		});
	}
	addToggler(obj){
		this.tElements.push(obj);
		obj.canvas.addEventListener('keyboardShowing', function(e){
			obj.toggle();
			if(obj instanceof TextInput){
				obj.cursorRecalc();
			}
		});
	}
	displayCmd(){
		this.ctx.fillStyle = "black";
		this.ctx.fillText(Canvas.createChunkWithPrepend(this, "p:<message>", 12), this.centerx/1.5, this.centery/3);
		this.t_multiplier = 2;
		this.ctx.font = "" + (15 * this.t_multiplier) + "px VT323";
		this.ctx.fillText(Canvas.createChunk(this, "-"), this.centerx, this.centery/1.75);
	}
}
