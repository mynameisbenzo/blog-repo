// self explanatory 
// plan on adding different effects that can be changed
/* 
	Example:  at the moment... stars load in the background, but
	I'd like to load in different effects.  Different backgrounds.  
	It would be nice to stick everything that involves that star 
	effect into its own class might work.  A scene class that loads 
	into the background and can be defined as you want with its 
	own purpose.  Could be cool!
*/
class Background extends Canvas{
	constructor(element){
		super(element);
		// Note:  this class is really convulted for my taste.  
		// the following could be apart of a background canvas class... possibly?
		this.canvas.style.height = '100%';
		this.canvas.style.width = '100%';
		
		this.loadBGEffect();
	}
	//current bg effect load in
	loadBGEffect(){
		this.bgeffect = new Stars(this);
	}
	/*
		Methods used to create circle
	*/
	createCircle(cr, cg, cb, crad, cxy){
		this.ctx.beginPath();
		this.ctx.moveTo(cxy.x, cxy.y);
		this.ctx.arc(cxy.x + 4, cxy.y + 4, crad, 0, 2*Math.PI);
		this.ctx.fillStyle = 'rgb('+ cr +', '+ cg +', '+ cb +')';
		this.ctx.fill();
	}
	firstStep(red, green, blue, radius, t, c){
		for(; radius > 0; radius--){
			setTimeout(this.createCircle.bind(this,red,green,blue,radius, c), t);
			red += 2, green += 1, blue += 20;
		}
	}
	switchBlackBlue(){
		var coord = {x:this.x, y:this.y};
		var r = 0, g = 0, b = 0, rad = 8, t = 50;
		for(; rad > 0; rad--){
			setTimeout(this.firstStep.bind(this,r,g,b,rad,t, coord), t);
			t+= 50;
		}
	}
	/*
		creates trail on canvas that follows mouse
	*/
	trail(e, obj){
		var rect = obj.canvas.getBoundingClientRect();
		obj.x = e.clientX/(rect.width/obj.canvas.width)-4, obj.y = e.clientY/(rect.height/obj.canvas.height)-4;
		obj.switchBlackBlue();
	}
}
class Scene{
	constructor(bck){
		this.bg = bck;
		this.dir = 'right';
		
		// default scene colors 
		this.start = 'white', this.end = 'black';
		
		// state 
		this.state = {};
		
		//new transition event
		this.wipeDone = new CustomEvent('wipe-done', {
			detail:{
				obj: this,
			}
		});
		
		//set event
		this.bg.canvas.addEventListener('wipe-done', function(e){
			e.detail.obj.performAction();
		});
	}
	performTransition(){
	}
	// for use in transitioning to new scene
	wipeRight(color, info=undefined){
		if(info === undefined){
			info = {};
			info['x'] = 0;
			info['trail'] = -10;
			info['time'] = 1;
		}else if(info['x'] > 101 && info['trail'] == 101) {
			this.bg.canvas.dispatchEvent(this.wipeDone);
			this.bg.ctx.fillRect(0, 0, this.bg.canvas.width, this.bg.canvas.height);
			this.end = color;
			this.bg.ctx.fillStyle = this.end;
			this.bg.ctx.fillRect(0, 0, this.bg.canvas.width, this.bg.canvas.height);
			return;
		}
		var grd = this.bg.ctx.createLinearGradient(info['x'], 0, this.bg.canvas.width, 0);
		if(info['trail'] > 0) grd.addColorStop(info['trail']/100, color);
		else grd.addColorStop(0, color);
		if(info['x'] < 100) grd.addColorStop(info['x']/100, this.end);
		else if(info['trail'] != 100) grd.addColorStop(1, this.end);
		if(info['x'] + 1 < 100) grd.addColorStop((info['x'] + 1)/100, this.end);
		this.bg.ctx.fillStyle = grd;
		this.bg.ctx.fillRect(0, 0, this.bg.canvas.width, this.bg.canvas.height);
		info['x']+=1;
		info['trail'] += 1;
		if(info['time'] < 15) info['time'] += 1;
		setTimeout(this.wipeRight.bind(this, color,  info), info['time']);
	}
	startWipe(bg){
		switch(this.dir){
			case 'right':
				setTimeout(this.wipeRight.bind(this, bg), 1);
				break;
			default:
				console.log("Direction not recognized.");
				break;
		}
	}
	static isColor(c){
		try{
			var x = document.createElement("CANVAS");
			var ctx = x.getContext('2d');
			var grd = ctx.createRadialGradient(0, 0, 0, 1, 1, 1);
			grd.addColorStop(0, c);
		}
		catch(err){
			console.log('Invalid color!');
			return false;
		}
		return true;
	}
}
class Stars extends Scene{
	constructor(bck, colorFrom='white', colorTo='black'){
		super(bck);
		this.start = colorFrom, this.end = colorTo;
		//canvas listener for star generator
		this.bg.canvas.addEventListener("star-end", function(e){
			if(e.detail.obj.drawnStars.length < e.detail.obj.maxStars) e.detail.obj.generateStars();
		});
		//newstar event
		this.makeStar = new CustomEvent('star-end', {
			detail:{
				obj: this,
			}
		});
		
		// star types
		this.starTypes = ['r', 'a'];
		this.state['star'] = 2;
		
		this.drawnStars = [];
		this.maxStars = 9;
		this.generateStars();
	}
	// for use in transitioning to new scene type
	bgChange(c){
		if(!Scene.isColor(c)) return;
		this.maxStars = 0;
		setTimeout(this.startWipe(c), 500);
	}
	performAction(){
		setTimeout(this.newBG.bind(this), 2000);
		this.maxStars = 9;
		this.generateStars();
	}
	newBG(){
		this.bg.ctx.fillStyle = this.end;
		this.bg.ctx.fillRect(0, 0, this.bg.canvas.width, this.bg.canvas.height)
	}
	/*
		Star generation
	*/
	radialStar(info){
		var grd = this.bg.ctx.createRadialGradient(info.x, info.y, 0, info.x+info.r, info.y+info.r, info.r);
		grd.addColorStop(0, this.start);
		grd.addColorStop(1, this.end);
		
		this.bg.ctx.fillStyle = grd;
		this.bg.ctx.fillRect(info.x, info.y, info.r, info.r);
		
		grd = this.bg.ctx.createRadialGradient(info.x, info.y, 0, info.x-info.r, info.y+info.r, info.r);
		grd.addColorStop(0, this.start);
		grd.addColorStop(1, this.end);
		
		this.bg.ctx.fillStyle = grd;
		this.bg.ctx.fillRect(info.x, info.y, -info.r, info.r);
		
		grd = this.bg.ctx.createRadialGradient(info.x, info.y, 0, info.x-info.r, info.y-info.r, info.r);
		grd.addColorStop(0, this.start);
		grd.addColorStop(1, this.end);
		
		this.bg.ctx.fillStyle = grd;
		this.bg.ctx.fillRect(info.x, info.y, -info.r,-info.r);
		
		grd = this.bg.ctx.createRadialGradient(info.x, info.y, 0, info.x+info.r, info.y-info.r, info.r);
		grd.addColorStop(0, this.start);
		grd.addColorStop(1, this.end);
		
		this.bg.ctx.fillStyle = grd;
		this.bg.ctx.fillRect(info.x, info.y, info.r, -info.r);
	}
	arcStar(info){
		this.bg.ctx.fillStyle = this.end;
		this.bg.ctx.fillRect(info.x - info.br, info.y - info.br, info.br * 2, info.br * 2);
		this.bg.ctx.beginPath();
		this.bg.ctx.moveTo(info.x + info.r, info.y);
		this.bg.ctx.quadraticCurveTo(info.x + info.r/16, info.y + info.r/16, info.x, info.y + info.r);
		this.bg.ctx.quadraticCurveTo(info.x - info.r/16, info.y + info.r/16, info.x - info.r, info.y);
		this.bg.ctx.quadraticCurveTo(info.x - info.r/16, info.y - info.r/16, info.x, info.y - info.r);
		this.bg.ctx.quadraticCurveTo(info.x + info.r/16, info.y - info.r/16, info.x + info.r,  info.y);
		this.bg.ctx.closePath();
		this.bg.ctx.fillStyle = this.start;
		this.bg.ctx.fill();
	}
	drawStar(info){
		if(info === undefined){
			info = this.generatePoint();
			this.drawnStars.push(info);
		}else if(info.r < 0){
			var i = this.drawnStars.indexOf(info);
			this.drawnStars.splice(i, 1);
			info = undefined;
			var t = getRandomInt(5000, 10000);
			if(this.drawnStars.length < this.maxStars - 1){
				setTimeout(this.bg.canvas.dispatchEvent(this.makeStar), t);
			}
			return;
		}
		(info['type'] == 'r') ? this.radialStar(info) : this.arcStar(info);
		var i = this.drawnStars.indexOf(info);
		this.drawnStars[i].r -= 1;
		setTimeout(this.drawStar.bind(this,this.drawnStars[i]), 100);
	}
	generateStars(){
		var t = getRandomInt(0, 5000);
		setTimeout(this.drawStar(), t);
		t = getRandomInt(0, 5000);
		setTimeout(this.drawStar(), t);
		t = getRandomInt(0, 5000);
		setTimeout(this.drawStar(), t);
		t = getRandomInt(0, 5000);
		setTimeout(this.drawStar(), t);
	}
	generatePoint(){
		var info = {}, done = false;
		while(!done){
			if(this.drawnStars.length < 1) done = true;
			info['r'] = getRandomInt(4,12);
			info['x'] = getRandomInt(0, this.bg.canvas.width-info['r']);
			info['y'] = getRandomInt(0, this.bg.canvas.height-info['r']);
			info['type'] = this.getStar();
			info['br'] = info['r'];
			this.drawnStars.every(function(element, index){
				if(info.x < element.x + element.br*2 &&
					info.x + info.br*2 > element.x &&
					info.y < element.y + element.br*2 &&
					info.y + info.br*2 > element.y){
						return true;
				}
				done = true;
				return false;
			});
		}
		return info;
	}
	getStar(){
		switch(this.state['star']){
			case 0:
			case 1:
				return this.starTypes[this.state['star']];
				break;
			default:
				return this.starTypes[getRandomInt(0, 1)];
		}
		return this.starTypes[getRandomInt(0, 1)]
	}
	setStar(type){
		switch(type){
			case 'radial':
				this.state['star'] = 0;
				break;
			case 'arc':
				this.state['star'] = 1;
				break;
			case 'mix':
				this.state['star'] = 2;
				break;
			default:
				break;
		}
	}
	setColor(c){
		if(Scene.isColor(c)) this.start = c;
	}
}
class Game extends Scene{
	constructor(back){
		super(bck);
	}
}