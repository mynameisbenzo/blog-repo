class Home{
	constructor(w){
		this.state = {
			textures:[],
			links:[],
			mY:0,
			mX:0,
			mD:false,
			currentFace:0,
		};

		this.scene = new THREE.Scene();
		this.camera = new THREE.PerspectiveCamera(
			45,
			window.innerWidth / window.innerHeight,
			1,
			750);
			
		this.camera.position.set(0,0,100);
		this.camera.lookAt(new THREE.Vector3(5,-25,0));
		
		this.faceFinder = new THREE.Raycaster();
		this.lastMouseTouch = new THREE.Vector2();
		
		this.m = new Modal($('#exModal'));
		// this.m.elem.modal('show');
		// this.light = new THREE.PointLight(0xff0000, 1, 10);
		// this.light.position.set(-5,0,0);
		// this.scene.add(this.light);
		
		// will use for rotating sprites later
		// var program = function(c){
			// c.beginPath();
			// c.arct(0,0,0.5,0,(Math.PI*2),true);
			// c.fill();
		// };
		// var cm = new THREE.SpriteMaterial({
			// color:0xff0000,
			// program:program
		// });
		// var sprite = new THREE.Sprite();
		// this.light.add(sprite);
		
		this.fGeo = new THREE.BoxGeometry(1000,1,500);
		this.fMat = new THREE.MeshPhongMaterial({color:0x333333});
		this.floor = new THREE.Mesh(this.fGeo, this.fMat);
		this.floor.receiveShadow = true;
		this.scene.add(this.floor);
		this.floor.position.set(0,-40,0);
		
		this.aLight = new THREE.AmbientLight(0xffffff, 0.2);
		this.scene.add(this.aLight);
		
		this.pLight = new THREE.PointLight(0xffffff, 1, 1);
		this.pLight.castShadow = true;
		this.pLight.shadow.camera.near = 0.1;
		this.pLight.shadow.camera.far = 25;
		this.pLight.position.set(30,50,50);
		this.scene.add(this.pLight);
		
		this.renderer = new THREE.WebGLRenderer();
		this.renderer.setSize(window.innerWidth, window.innerHeight);
		this.renderer.domElement.id = 'resize';
		this.renderer.domElement.style.width = "100%";
		this.renderer.domElement.style.height = "100%";
		this.renderer.shadowMap.enabled = true;
		this.renderer.shadowMap.type = THREE.BasicShadowMap;
		document.body.appendChild(this.renderer.domElement);
		
		this.setListeners(this, w);
		Home.readjustElement(document.getElementById('navStyleCanvas'));
				Home.readjustElement(document.getElementById('currentFace'), true);
	}
	loadLinks(links){
		this.state.links = links;
	}
	loadTextures(textures){
		this.state.textures = textures; 
	}
	createCube(){
		this.rgeometry = new THREE.BoxGeometry(40,40,40, 20, 20, 8);
		var radius = 4;
		var width = 40,
			height = 40;
		var v1 = new THREE.Vector3();
		var w1 = (width - (radius * 2)) * 0.5,
		h1 = (height - (radius * 2)) * 0.5;
		var vTemp = new THREE.Vector3(),
		vSign = new THREE.Vector3(),
		vRad = new THREE.Vector3();
		this.rgeometry.vertices.forEach(v => {
			v1.set(w1, h1, v.z);
			vTemp.multiplyVectors(v1, vSign.set(Math.sign(v.x), Math.sign(v.y), 1));
			vRad.subVectors(v, vTemp);
			if (Math.abs(v.x) > v1.x && Math.abs(v.y) > v1.y && vRad.length() > radius) {
				vRad.setLength(radius).add(vTemp);
				v.copy(vRad);
			}
		});
		
		this.cubeMats = [];
		for(var i = 0; i < this.state.textures.length; i ++){
			var mat = new THREE.MeshPhongMaterial({
					map: new THREE.TextureLoader().load(this.state.textures[i]),
					side: THREE.DoubleSide,
					});
			mat.transparent = true;
			mat.opacity = 0;
			mat.receiveShadow = true;
			this.cubeMats.push(mat);
		}
		
		this.rcube = new THREE.Mesh(this.rgeometry, this.cubeMats);
		this.rcube.position.set(0,0,0);
		this.rcube.rotation.y = Math.PI/2;
		this.scene.add(this.rcube);
		this.pLight.lookAt(this.rcube.position);
	}
	setListeners(o,w){
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			w.addEventListener('touchmove', function(e){
				if(o.m.state.visMain) return;
				if(o.state.mD){
					var dX = e.touches[0].clientX - o.state.mX, dY = e.touches[0].clientY - o.state.mY;
					o.state.mY = e.touches[0].clientY;
					o.state.mX = e.touches[0].clientX;
					var rotateThisWay = new THREE.Quaternion().setFromEuler(new THREE.Euler(
						UTILS.toRadians(dY * 1),
						UTILS.toRadians(dX * 1),
						0,
						'XYZ'));
					o.rcube.quaternion.multiplyQuaternions(rotateThisWay, o.rcube.quaternion);
				}
			});
			w.addEventListener('touchstart', function(e){
				if(o.m.state.visMain) return;
				if(e.target.id == 'resize'){
					o.state.mX = e.touches[0].clientX;
					o.state.mY = e.touches[0].clientY;
					o.state.mD = true;
				}
			});
			w.addEventListener('touchend', function(e){
				if(o.m.state.visMain) return;
				o.state.mD = false;
				o.checkFace();
			});
		}else{
			w.addEventListener('mouseup', function(e){
				if(o.m.state.visMain) return;
				o.state.mD = false;
				o.checkFace();
			});
			w.addEventListener('mousedown', function(e){
				if(o.m.state.visMain) return;
				if(e.target.id == 'resize'){
					o.state.mX = e.clientX;
					o.state.mY = e.clientY;
					o.state.mD = true;
				}
			});
			w.addEventListener('mousemove', function(e){
				if(o.m.state.visMain) return;
				if(o.state.mD && o.rcube != undefined){
					var dX = e.offsetX - o.state.mX, dY = e.offsetY - o.state.mY;
					o.state.mY = e.offsetY;
					o.state.mX = e.offsetX;
					var rotateThisWay = new THREE.Quaternion().setFromEuler(new THREE.Euler(
						UTILS.toRadians(dY * 1),
						UTILS.toRadians(dX * 1),
						0,
						'XYZ'));
					o.rcube.quaternion.multiplyQuaternions(rotateThisWay, o.rcube.quaternion);
				}
			});
			w.addEventListener('resize', function() {
				o.camera.aspect = w.innerWidth / w.innerHeight;
				o.camera.updateProjectionMatrix();
				o.renderer.setSize(w.innerWidth, w.innerHeight);
				Home.readjustElement(document.getElementById('navStyleCanvas'));
				Home.readjustElement(document.getElementById('currentFace'), true);
			});
		}
	}
	updateCube(){
		if(this.cubeMats[0].opacity < 1){
			for(var i = 0; i < this.cubeMats.length; i++){
				this.cubeMats[i].opacity += 0.01;
			}
		}else if(this.pLight.distance < 1000){
			this.pLight.distance += 1;
		}
	}
	checkFace(){
		var mouse = {x:0,y:0}
		this.faceFinder.setFromCamera( mouse, this.camera );   

		var intersects = this.faceFinder.intersectObject( this.rcube);
		var faceIndex = intersects[0].faceIndex;
		var mi = intersects[0].object.geometry.faces[faceIndex].materialIndex;
		var src = intersects[0].object.material[mi].map.image.src;
		var pieces = src.split('/');
		pieces = pieces.slice(3,pieces.length);
		pieces[0] = "/" + pieces[0];
		src = pieces.join('/');
		this.state.currentFace = this.state.textures.indexOf(src);
		this.updateElement(document.getElementById('currentFace'));
	}
	
	updateElement(elem){
		var o = this;
		switch(this.state.currentFace){
			case 0:
				elem.innerHTML = "Blogs";
				elem.onclick = null;
				break;
			case 1:
				elem.innerHTML = "Experience";
				elem.onclick = function(e){
					o.m.setContent('experience');
					o.m.goDirection('right');
					e.stopPropagation();
					e.preventDefault();
				}
				break;
			case 2:
				elem.innerHTML = "LinkedIn";
				elem.onclick = null;
				break;
			case 3:
				elem.innerHTML = "Contact";
				elem.onclick = function(e){
					o.m.setContent('contact');
					o.m.goDirection('right');
					e.stopPropagation();
					e.preventDefault();
				}
				break;
			case 4:
				elem.innerHTML = "Projects";
				elem.onclick = function(e){
					o.m.setContent('projects');
					o.m.goDirection('right');
					e.stopPropagation();
					e.preventDefault();
				}
				break;
			case 5:
				elem.innerHTML = "About";
				elem.onclick = function(e){
					o.m.setContent('about');
					o.m.goDirection('right');
					e.stopPropagation();
					e.preventDefault();
				}
				break;
			default:
				break;
		}
		elem.parentElement.href = this.state.links[this.state.currentFace];
	}
	 /*************************************************
		Static functions
	 **************************************************/
	
	static readjustElement(elem, centerMid=false){
		if(centerMid) {
			elem.style.position = 'fixed';
			elem.style.left = (window.innerWidth/2 - elem.clientWidth/2 ) + "px";
			elem.style.fontSize = "200%";
			elem.style.borderRadius = "5px";
			elem.style.backgroundColor = '#6CA0B5';
			elem.style.padding = "5px";
		}
		elem.style.top = (window.innerHeight - elem.clientHeight - 5) + "px";
		elem.style.zindex = '10';
	}
	


	animate(){
		if(this.rcube != undefined) {
			this.rcube.position.set(0,0,0);
			this.updateCube();
		}
		this.camera.position.set(-10,25,100);
		this.m.update();
		requestAnimationFrame(this.animate.bind(this));
		this.renderer.render(this.scene, this.camera);
	}
}
var h = new Home(window);
h.animate();

var tout;
$('#hoverExpand').hover(function(){
	if(!$('#nav-dropdown').is(':visible')) $('#nav-dropdown').slideDown('slow');
});
$('#hoverExpand').click(function(e){
	e.stopPropagation();
	e.preventDefault();
});
$('#hoverExpand').mouseleave(function(e){
	tout = setTimeout(function(){
		$('#nav-dropdown').slideUp('slow');
	}, 250);
});
$('#nav-dropdown').mouseenter(function(){
	clearTimeout(tout);
});
$('#nav-dropdown').mouseleave(function(){
	$('#nav-dropdown').slideUp('slow');
});
