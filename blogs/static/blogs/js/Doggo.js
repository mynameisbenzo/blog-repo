class GameObject{
	constructor(g){
	}
}
class Player{
	constructor(g){
	}
}
class Game{
	constructor(w){
		this.state = {
			arr:true,
			cAClip:-1,
			cTClip:-1,
			cMClip:-1,
			next:0,
		};
		
		this.gameOver = document.getElementById('dead');
		this.soundBtn = document.getElementById('pauseplay');
		
		this.scene = new THREE.Scene();
		this.camera = new THREE.PerspectiveCamera(
			45,
			window.innerWidth / window.innerHeight,
			1,
			500);
			
			
		this.camera.position.set(0,2,-10);
		this.camera.lookAt(new THREE.Vector3(0,0,0));
		
		this.light = new THREE.HemisphereLight(0xbbbbff, 0x444422);
		this.light.position.set(0, 2,0);
		this.scene.add(this.light);
		
		this.renderer = new THREE.WebGLRenderer();
		this.renderer.setSize(window.innerWidth, window.innerHeight);
		this.renderer.gammaOutput = true;
		this.renderer.domElement.id = 'getHeight';
		document.body.appendChild(this.renderer.domElement);
		
		// floor
		this.floorGeometry = new THREE.BoxGeometry(100, 100, 1);
		this.floorColor = new THREE.Color(1,1,1);
		this.floorMaterial = new THREE.MeshBasicMaterial({color:this.floorColor});
		this.floor = new THREE.Mesh(this.floorGeometry, this.floorMaterial);
		this.floor.position.setScalar(0,0,0);
		this.scene.add(this.floor);
		
		this.doggo = null;
		this.headBone = null;
		this.dActions = this.dTailActions = this.dMouthActions = [];
		
		this.clock = new THREE.Clock(false);
		this.aniMixer = this.tailMixer = this.mouthMixer = null;
		this.action = null;
	}
	setEnvMap(m){
		this.EM = new THREE.CubeTextureLoader().load([
			m,m,m,m,m,m
		],
		function(m){
			console.log(m);
		},
		function(m){
			console.log((xhr.loaded/xhr.total * 100) + "% loaded");
		},
		function(m){
			console.log('could not load envmap');
		});
	}
	loadObj(obj, type){
		var loader = new THREE.GLTFLoader();
		var o = this;
		switch(type){
			case 'doggo':
				loader.load(obj,function(object){
					o.dAnimate = object.scene;
					o.doggo = object.scene.children[0];
					o.dActions.push(object.animations[1]);
					o.dActions.push(object.animations[2]);
					o.dActions.push(object.animations[3]);
					o.dTailActions.push(object.animations[0]);
					o.dTailActions.push(object.animations[5]);
					o.dMouthActions.push(object.animations[4]);
					
					o.dClips = object.animations;
					o.placeDoggo();
					},function(xhr){
						console.log((xhr.loaded/xhr.total * 100) + "% loaded");
					},function(error){
						console.log('could not load object');
					}
				);
				break;
			default:
				break;
		}
	}
	placeDoggo(){
		this.scene.add(this.doggo);
		this.aniMixer = new THREE.AnimationMixer(this.doggo);
		this.tailMixer = new THREE.AnimationMixer(this.doggo);
		this.mouthMixer = new THREE.AnimationMixer(this.doggo);
		this.headBone = this.doggo.children[2].children[0];
		this.headBone.rotateX(Game.toRad(15));
		this.doggo.rotateY(Game.toRad(180));
		this.camera.lookAt(this.doggo.position);
		this.light.position.set(
			this.doggo.position.x, 
			this.doggo.position.y + 1,
			this.doggo.position.z);
		console.log(this.dClips);
		this.playAnimation('action','Standing');
	}
	playAnimation(type,clipName){
		switch(type){
		case 'action':
			if(this.aniMixer._actions.length > 0){
				this.aniMixer.uncacheClip(this.dClips[this.state.cAClip]);
			}
			for(var i = 0; i < this.dClips.length; i++){
				if(this.dClips[i].name == clipName){
					this.aniMixer.clipAction(this.dClips[i]).play();
					this.state.cAClip = i;
				}
			}
			break;
		case 'tail':
			if(this.tailMixer._actions.length > 0){
				this.tailMixer.uncacheClip(this.dClips[this.state.cTClip]);
			}
			for(var i = 0; i < this.dClips.length; i++){
				if(this.dClips[i].name == clipName){
					this.tailMixer.clipAction(this.dClips[i]).play();
					this.state.cTClip = i;
				}
			}
			break;
		case 'mouth':
			if(this.mouthMixer._actions.length > 0){
				this.mouthMixer.uncacheClip(this.dClips[this.state.cMClip]);
			}
			for(var i = 0; i < this.dClips.length; i++){
				if(this.dClips[i].name == clipName){
					this.mouthMixer.clipAction(this.dClips[i]).play();
					this.state.cMClip = i;
				}
			}
			break;
		default:
			break;
		}
	}
	 /*************************************************
		Static functions
	 **************************************************/
	 //https://discourse.threejs.org/t/functions-to-calculate-the-visible-width-height-at-a-given-z-depth-from-a-perspective-camera/269
	 static visibleWidthAtZDepth( depth, camera ){
		const height = Game.visibleHeightAtZDepth( depth, camera );
		return height * camera.aspect;
	}
	static visibleHeightAtZDepth( depth, camera ) {
		// compensate for cameras not positioned at z=0
		const cameraOffset = camera.position.z;
		if ( depth < cameraOffset ) depth -= cameraOffset;
		else depth += cameraOffset;

		// vertical fov in radians
		const vFOV = camera.fov * Math.PI / 180; 

		// Math.abs to ensure the result is always positive
		return 2 * Math.tan( vFOV / 2 ) * Math.abs( depth );
	}
	
	//https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
	static getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	static normalize(value, start, end){
		return (value - start)/(end-start);
	}
	static toRad(d){
		return d * (Math.PI/180);
	}
	// static uiAdjust(elem){
		// switch(elem.id){
			// case 'scoreDisplay':
				// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					// elem.style.left = "5%";
				// }
				// break;
			// case 'dead':
				// if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
					// elem.style.fontSize = "300%";
				// }
				// elem.style.left = document.body.clientWidth/2 - elem.clientWidth/2 + "px";
				// elem.style.top = "15%";
				// break;
			// case 'pauseplay':
				// elem.style.top = window.innerHeight - elem.clientHeight* 2 - 20 +"px";
				// elem.style.left = document.body.clientWidth/2 - elem.clientWidth/2 + "px";
				// break;
			// default:
				// break;
		// }
	// }
	static mobileCheck(){
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) return true;
		return false;
	}
	//main loop
	animate(){
		if(this.aniMixer){
			if(!this.clock.running) this.clock.start();
			this.aniMixer.update(this.clock.getDelta());
			if(this.clock.getDelta() > 0){
				this.mouthMixer.update(this.state.next/5000);
				this.tailMixer.update(this.state.next/5000);
			}
			console.log(this.clock.getDelta());
			this.doggo.rotation.y = Game.toRad(0);
			if(this.clock.getElapsedTime() > 3){
				this.playAnimation('mouth','Breathing');
				this.playAnimation('tail','Tailwag1');
				this.playAnimation('action','StandToSit');
				this.clock.stop();
				this.clock.start();
				this.state.next = 0;
			}
			this.state.next += 1;
		}
		if(this.doggo){
			this.floor.position.x = this.doggo.position.x;
			this.floor.position.y = this.doggo.position.y;
			this.floor.position.z = this.doggo.position.z + 100;
			this.camera.lookAt(this.doggo.position);
		}
		requestAnimationFrame(this.animate.bind(this));
		this.renderer.render(this.scene, this.camera);
		
	}
}
var g = new Game(window);
g.animate();