from django.urls import path

from . import views

app_name = "blogs"

urlpatterns = [
	# canvas page
	path('canvas/', views.canvas, name='secret'),

	# actual dontpushme page
	path('dontpushme/', views.dontpushme, name="dontpushme"),

	# robo doggo
	path('roboDoggo/', views.dog, name="robodog"),

	# birthday!
	path('birthday/', views.birthday, name="birthday"),

	# home page
	path('', views.home, name='index'),

	# blogs page
	path('blogs/', views.blogs, name='blogs'),

	# add new post
	path(app_name + '/new_post/', views.new_post, name='new_post'),

	# edit previous posts
	path(app_name + '/edit_post/<post_id>/', views.edit_post, name='edit_post'),
]
