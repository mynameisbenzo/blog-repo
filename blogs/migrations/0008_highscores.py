# Generated by Django 2.0.6 on 2018-07-21 23:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0007_tickercalls'),
    ]

    operations = [
        migrations.CreateModel(
            name='HighScores',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('game', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=5)),
                ('score', models.IntegerField()),
            ],
            options={
                'verbose_name_plural': 'highscores',
            },
        ),
    ]
