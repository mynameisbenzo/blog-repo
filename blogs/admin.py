from django.contrib import admin

from blogs.models import BlogPost, NewUserRequest, PictureInfo, TickerCalls, HighScores

admin.site.register(BlogPost)
admin.site.register(NewUserRequest)
admin.site.register(PictureInfo)
admin.site.register(TickerCalls)
admin.site.register(HighScores)