import random
import string
import secrets
import os
import json

'''
	creates a random string of words to be used as keys
'''
def random_string():
	words = [
		'Very','Silly','Alligator','Elephant','Dog','Cat','Magenta',
		'Purple','Oscar','Ping','Pong','Giant','Football','Baseball',
		'Hippopotamus','Car','Truck','Bingo','Awkward','Thunder',
		'Bull','Cavalier','Rose','Wagon','Horse','Pony','Desk','Scary',
		'Rich','Switch','Funny','Exuberant','Fantastic','Great','Good',
		'Comfy','Hilarious','Queazy','Blonde','Black','White','Fast',
		'Slow','Endangered','Blue','Pink','Green','Orange','Place'
	]
	return (''.join(secrets.choice(words) for _ in range(15)))
	
	
'''
	initial creation of keys for user creation
'''	
def init_keys():
	filename = 'my_keys.json'
	
	if os.stat(filename).st_size != 0:
		return
	
	try:
		with open(filename, 'w') as my_j:
			my_keys = {}
			for _ in range(25):
				my_keys[random_string()] = 0
			
			json.dump(my_keys, my_j)
	except Exception as e:
		print(e)