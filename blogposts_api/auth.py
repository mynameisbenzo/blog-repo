from tastypie.authentication import Authentication

# custom authentication only allows for get calls
class OnlyGETAuth(Authentication):
	def is_authenticated(self, request, **kwargs):
		if request.method == 'GET':
			return True
		return False
		
	def get_identifier(self, request):
		return None