from tastypie.resources import ModelResource
from tastypie.authentication import BasicAuthentication, Authentication


from blogs.models import BlogPost, PictureInfo
from portfolio.models import ExperienceEntry, ProjectEntry
from .models import Phrases
from .auth import OnlyGETAuth


# api resource for blogposts 
class BlogPostResource(ModelResource):
	class Meta:
		queryset = BlogPost.objects.all()
		resource_name = 'blogposts'
		excludes = ['id']
		authentication = BasicAuthentication()
		
# api resource for phrases
class PhrasesResource(ModelResource):
	class Meta:
		queryset = Phrases.objects.all()
		filtering = {
			"type": ['exact',],
		}
		ordering = ['type']
		resource_name = "phrases"
		excludes = ['id']
		authentication = OnlyGETAuth()
		
# used to grab pictures for front page carousel
class PictureInfoResources(ModelResource):
	class Meta:
		queryset = PictureInfo.objects.all()
		resource_name = 'pictures'
		authentication = OnlyGETAuth()

# used to grab project/experience entry
class ProjectResource(ModelResource):
	class Meta:
		queryset = ProjectEntry.objects.all().order_by('-id')
		resource_name = 'projects'
		authentication = OnlyGETAuth()
		
class ExperienceResource(ModelResource):
	class Meta:
		queryset = ExperienceEntry.objects.all().order_by('-id')
		resource_name = 'experience'
		authentication = OnlyGETAuth()