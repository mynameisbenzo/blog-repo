from django.shortcuts import render

# Create your views here.
def documentation(request):
	return render(request, 'blogposts_api/api_usage.html')