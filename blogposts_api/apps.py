from django.apps import AppConfig


class BlogpostsApiConfig(AppConfig):
    name = 'blogposts_api'
