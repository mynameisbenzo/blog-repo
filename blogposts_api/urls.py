from django.urls import path

from . import views

urlpatterns = [
	#documentation
	path('api/documentation', views.documentation, name="documentation"),
]