from django.db import models

PHRASE_TYPES = (
	('tips', 'TIPS'),
	('about_me', 'ABOUT'),
	('random_fact', 'RANDOMFACT'),
	('hints','HINTS'),
	('stocks','STOCKS')
)

# for use in loading canvas phrases loading 
class Phrases(models.Model):
	type = models.CharField(max_length=10, choices=PHRASE_TYPES, default='tips')
	text = models.TextField()
	
	class Meta:
		verbose_name_plural = 'phrases'
	
	def __str__(self):
		return self.type + " " + self.text 