from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
	url(r'wss/portfolio/projects/ticker', consumers.TickerConsumer),
	url(r'wss/portfolio/projects/stock_chat', consumers.ChatConsumer),
	url(r'ws/portfolio/projects/ticker', consumers.TickerConsumer),
	url(r'ws/portfolio/projects/stock_chat', consumers.ChatConsumer),
]