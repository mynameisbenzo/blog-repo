# chat/urls.py
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'portfolio/projects/ticker', views.ticker, name='ticker'),
]