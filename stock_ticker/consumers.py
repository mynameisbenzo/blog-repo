from django.conf import settings
from channels.generic.websocket import AsyncWebsocketConsumer

from blogs.models import TickerCalls

import json, requests, re

class ChatConsumer(AsyncWebsocketConsumer):
	async def connect(self):
		self.room_group_name = 'ticker_chat'
		
		# join
		await self.channel_layer.group_add(
			self.room_group_name,
			self.channel_name
		)
		
		await self.accept()
		
	async def disconnect(self, close_code):
		# leave
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
		)
		
	# receive message from socket
	async def receive(self, text_data):
		text_data_json = json.loads(text_data)
		message = text_data_json['stock_chat_message']
		
		# send message to room group
		await self.channel_layer.group_send(
			self.room_group_name,
			{
				'type': 'stock_chat_message',
				'stock_chat_message': message
			}
		)
		
	# receive message from room group
	async def stock_chat_message(self, event):
		message = event['stock_chat_message']
		# send message to websocket
		await self.send(text_data=json.dumps({
			'stock_chat_message': message
		}))

class TickerConsumer(AsyncWebsocketConsumer):
	async def connect(self):
		self.room_group_name = 'ticker'
		
		# join room group
		await self.channel_layer.group_add(
			self.room_group_name,
			self.channel_name
		)
		
		await self.accept()
		
	async def disconnect(self, close_code):
		# leave room group
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
		)
		
	# receive message from websocket
	async def receive(self, text_data):
		text_data_json = json.loads(text_data)
		if 'message' not in text_data_json:
			return
		message = text_data_json['message']
		
		test_rel = {}
		high, low = [], []
		for m in range(len(message)):
			high.append(0)
			low.append(0)
			r = requests.get(
				"https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="
				 + message[m].upper() + 
				 "&interval=5min&apikey=" + 
				 self.return_and_recycle())
			
			if re.search('html', r.text) != None:
				print('alpha avantage api call failed: connection problems')
				test_rel = self.request_error('Ticker call currently under maintenance')
				break
			testing = json.loads(r.text)
			if 'Error Message' not in testing:
				test_rel[message[m].upper()] = {}
				for e,v in testing['Time Series (5min)'].items():
					price = float(v['2. high']) 
					if price > high[m]:
						high[m] = price
					elif price < low[m] or low[m] == 0:
						low[m] = price
					test_rel[message[m].upper()][e] = price
				test_rel[message[m].upper()]['high'] = high[m]
				test_rel[message[m].upper()]['low'] = low[m]
				print('success')
				if TickerCalls.objects.filter(ticker=message[m].upper()).exists():
					t = TickerCalls.objects.get(ticker=message[m].upper())
					t.amt += 1
					t.save()
				else:
					TickerCalls.objects.create(ticker=message[m].upper(),amt=1)
			else:
				test_rel = self.request_error('Ticker cannot be found.')
				print('api call fail on : ' + str(message[m]).upper())
		
		#send message to room group
		await self.channel_layer.group_send(
			self.room_group_name,
			{
				'type': 'ticker_message',
				'message': json.dumps(test_rel)
			}
		)
		
	# receive message from room group
	async def ticker_message(self, event):
		message = event['message']
		
		# send message to websocket
		await self.send(text_data=json.dumps({
			'message' : message
		}))
		
	# return error json
	def request_error(self, msg):
		obj = {}
		obj['symbol'] = 'ERROR'
		obj['ERR_message'] =  msg
		return obj
		
	# recycle api keys
	def return_and_recycle(self):
		k = settings.ALPHA_ADV_API_KEYS[0]
		settings.ALPHA_ADV_API_KEYS.remove(k)
		settings.ALPHA_ADV_API_KEYS.append(k)
		return k