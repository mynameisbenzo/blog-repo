from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
	
from .utils import pick_key, verify_key, replace_key

from blogs.models import NewUserRequest
	
'''
	create new user
'''
def register(request):
	'''
		insure user isn't coming here directly without going through
		key verification.
	'''
	try:
		new_user_request = NewUserRequest.objects.get(key=request.session['key'])
	except Exception as e:
		print(e)
		raise Http404
		
	if request.method != 'POST':
		# blank form
		form = UserCreationForm()
	else:
		# process completed form
		form = UserCreationForm(data=request.POST)
		
		if form.is_valid():
			new_user = form.save()
			# log user in and redirect to home page
			authenticated_user = authenticate(username=new_user.username,
				password=request.POST['password1'])
			login(request, authenticated_user)
			
			replace_key(request.session['key'])
			
			request.session['key'] = ''
			request.session['redirect_check'] = ''
			new_user_request.delete()
			
			return HttpResponseRedirect(reverse('index'))
		
	context = {'form':form}
	return render(request, 'users/register.html', context)
	
'''
	send email request for key
'''
def send_email(request):
	if request.method == 'POST':
		if 'email-to' in request.POST:
			if request.POST['email-to'] == '':
				raise Http404
				
			key_request = NewUserRequest()
			key_request.email = request.POST['email-to']
			key_request.key = pick_key()
			key_request.user_exists = False
			
			key_request.save()
			return HttpResponseRedirect(reverse('thank-you'))
		elif 'given-key' in request.POST:
			print(verify_key(request.POST['given-key']))
			if verify_key(request.POST['given-key']):
				request.session['redirect_check'] = 'not empty'
				request.session['key'] = request.POST['given-key']
				return HttpResponseRedirect(reverse('register'))
			return HttpResponseRedirect(reverse('index'))
	return render(request, 'users/request_access.html')
	
'''
	confirm email was sent
'''
def thank_you(request):
	return render(request, 'users/thank_you.html')