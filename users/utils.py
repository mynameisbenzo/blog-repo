import json
import secrets


'''
	pick key at random from json file
	NOTE:
	key states: 
		0: unused
		1: requested
		
	in replace key, if key value = 1, then
	replace key.
'''
def pick_key():
	try:
		# open and grab key
		with open('my_keys.json') as my_j:
			stored_keys = json.load(my_j)
			
			single_key = secrets.choice(list(stored_keys.items()))
			
			stored_keys[single_key[0]] = 1
			key = single_key[0]
			
			my_j.close()
			
		# saved grabbed key value to json file as requested
		with open('my_keys.json', 'w') as my_j2:
			json.dump(stored_keys, my_j2)
			my_j2.close()
		return key
	except Exception as e:
		print(e)
		return 'BADA BOOM'	
		
'''
	check if given key exists and is in the correct state
'''
def verify_key(key):
	try:
		with open('my_keys.json') as my_j:
			stored_keys = json.load(my_j)
			if key in stored_keys and stored_keys[key] == 1:
				return True
			return False
	except Exception as e:
		print(e)
		return False
		
'''
	replace key with new key
'''
def replace_key(key):
	try:
		# find key and remove from json if in requested state
		with open('my_keys.json') as my_j:
			stored_keys = json.load(my_j)
			
			if key not in stored_keys:
				return False
				
			stored_keys.pop(key, None)
			stored_keys[random_string()] = 0
			
			my_j.close()
			
		# store new json in json file
		with open('my_keys.json', 'w') as my_j2:
			json.dump(stored_keys, my_j2)
			my_j2.close()
		
		return True
	except Exception as e:
		print(e)
		return False
'''
	generate single key
'''
def random_string():
	words = [
		'Very','Silly','Alligator','Elephant','Dog','Cat','Magenta',
		'Purple','Oscar','Ping','Pong','Giant','Football','Baseball',
		'Hippopotamus','Car','Truck','Bingo','Awkward','Thunder',
		'Bull','Cavalier','Rose','Wagon','Horse','Pony','Desk','Scary',
		'Rich','Switch','Funny','Exuberant','Fantastic','Great','Good',
		'Comfy','Hilarious','Queazy','Blonde','Black','White','Fast',
		'Slow','Endangered','Blue','Pink','Green','Orange','Place'
	]
	return (''.join(secrets.choice(words) for _ in range(15)))