// readjusting necessary parts
/*
	w is for viewport width
	t for top (picture), u for undertop (blogs), tl my name, tr for image location, pj for project section
	left/right for carousel buttons
*/
// var t, ut, tl, br, w, pj, left, right, i = 1;

class Home{
	constructor(w){
		this.state = {
			'clickStatus':false,
			'interval': null,
			'imgBuffer': null,
			'locationBuffer': null,
			'toLoad': null,
			'clearThenLoad':null,
		};
		
		this.refwin = document.body;
		this.index = 1;
		this.toppic = document.getElementById('topbg');
		this.blogs = document.getElementById('blogpost');
		this.projects = document.getElementById('projectexample');
		this.name = document.getElementById('text-top-left');
		this.piclocation = document.getElementById('text-bottom-right');
		this.leftbutton = document.getElementById('goLeft'), this.rightbutton = document.getElementById('goRight');
		this.hWidth =this.refwin.clientWidth;
		this.toppic.style.top = document.getElementById('thisSize').clientHeight + 10 + "px";
		this.toppic.style.left =this.refwin.clientWidth/2 - this.toppic.clientWidth/2 + "px";
		//configure carousel buttons
		this.leftbutton.style.top = document.getElementById('thisSize').clientHeight + 10 + this.toppic.clientHeight/2 - this.leftbutton.clientHeight/2 + "px";
		this.rightbutton.style.top = this.leftbutton.style.top;
		this.leftbutton.style.left = "10px";
		this.rightbutton.style.left =this.refwin.clientWidth - 10 - this.rightbutton.clientWidth + "px";
		//align blogs
		this.blogs.style.top = this.toppic.clientHeight + 75 + "px";
		this.blogs.style.left =this.refwin.clientWidth/2 - this.blogs.clientWidth/2 + "px";
		//align projects
		this.projects.style.top = this.toppic.clientHeight + 75 + this.blogs.clientHeight + "px";
		this.projects.style.left = this.blogs.style.left;
		//align top image caption
		this.name.style.left =this.refwin.clientWidth/2 - this.name.clientWidth/2 -10 + "px";
		this.name.style.top = document.getElementById('thisSize').clientHeight + 30 + "px";
		//align bottom image caption
		this.piclocation.style.top = document.getElementById('thisSize').clientHeight + this.toppic.clientHeight - this.piclocation.clientHeight + "px";
		this.piclocation.style.left =this.refwin.clientWidth/2 - this.piclocation.clientWidth/2 - 10 + "px";
		
		//queued messages is empty
		this.test = new CustomEvent('test', {
			detail:{
				str: null,
			}
		});
	}
	assignListeners(o){
		this.leftbutton.addEventListener('test', function(e){
			if(e.detail.str != null){
				o.state.imgBuffer = e.detail.str.link;
				o.state.locationBuffer = e.detail.str.location
			}
			switch(o.state.toLoad){
				case 'left':
					o.toppic.style.left = (o.refwin.clientWidth) + "px";
					o.state.interval = setInterval(o.goLeft.bind(this, o, o.toppic, true), 10)
					break;
				case 'right':
					o.toppic.style.left = (o.refwin.clientWidth * -1) + "px";
					o.state.interval = setInterval(o.goRight.bind(this, o, o.toppic, true), 10)
					break;
				case 'done':
					o.toppic.src = "";
					o.state.clearThenLoad = true;
					o.toppic.src = o.state.imgBuffer;
					o.state.clearThenLoad = false;
					break;
				default:
					break;
			}
		});
		this.leftbutton.addEventListener('click', function(e){
			o.getPicture(o,false);
		});
		this.rightbutton.addEventListener('click', function(e){
			o.getPicture(o);
		});
		this.toppic.addEventListener('load', function(e){
			if(!o.state.clearThenLoad){
				o.newPicture();
				o.reAdjust();
			}
		});
	}
	reAdjust(){
		if(!this.state.clickStatus) {
			this.toppic.style.left = this.refwin.clientWidth/2 - this.toppic.clientWidth/2 + "px";
		}
		this.toppic.style.top = document.getElementById('thisSize').clientHeight + 10 + "px";
		this.leftbutton.style.top = document.getElementById('thisSize').clientHeight + 10 + this.toppic.clientHeight/2 - this.leftbutton.clientHeight/2 + "px";
		this.rightbutton.style.top = this.leftbutton.style.top;
		this.leftbutton.style.left = "10px";
		this.rightbutton.style.left =this.refwin.clientWidth - 10 - this.rightbutton.clientWidth + "px";
		this.blogs.style.top =this.toppic.clientHeight + 75 + "px";
		this.blogs.style.left =this.refwin.clientWidth/2 - this.blogs.clientWidth/2 + "px";
		this.projects.style.top =this.toppic.clientHeight + 75 + this.blogs.clientHeight + "px";
		this.projects.style.left = this.blogs.style.left;
		this.name.style.left =this.refwin.clientWidth/2 - this.name.clientWidth/2 -10 + "px";
		this.name.style.top = document.getElementById('thisSize').clientHeight + 30 + "px";
		this.piclocation.style.top = document.getElementById('thisSize').clientHeight +this.toppic.clientHeight - this.piclocation.clientHeight + "px";
		this.piclocation.style.left =this.refwin.clientWidth/2 - this.piclocation.clientWidth/2 + "px";
	}
	newPicture(){
		this.toppic.style.left =this.refwin.clientWidth/2 - this.toppic.clientWidth/2 + "px";
		this.toppic.style.top = document.getElementById('thisSize').clientHeight + 10 + "px";
		this.piclocation.style.top = document.getElementById('thisSize').clientHeight + this.toppic.clientHeight - this.piclocation.clientHeight + "px";
		this.piclocation.style.left =this.refwin.clientWidth/2 - this.piclocation.clientWidth/2 + "px";
		this.state.clickStatus = false;
		this.piclocation.innerHTML = this.state.locationBuffer;
	}
	getPicture(o,increment=true){
		if(o.state.clickStatus) return;
		o.state.clickStatus = true;
		o.piclocation.innerHTML = '';
		(increment) ? o.state.interval = setInterval(o.goRight.bind(this, o, o.toppic), 10) 
			: o.state.interval = setInterval(o.goLeft.bind(this, o, o.toppic), 10);
		(increment) ? o.index += 1 : o.index -= 1;
		var url = window.location;
		var base = url.protocol + "//" + url.host + "/" + url.pathname.split('/')[0];
		var xobj = new XMLHttpRequest(), rand_call = new XMLHttpRequest();
		xobj.overrideMimeType("application/json"), rand_call.overrideMimeType('application/json');
		xobj.open('GET', base + 'api/v1/pictures/?format=json', true);
		xobj.send(null);
		xobj.onload  = function(){
			var info = JSON.parse(xobj.response);
			(o.index > info['meta']['total_count']) ? o.index = 1 : 
				(o.index < 1) ? o.index = info['meta']['total_count'] : null;
			
			rand_call.open('GET', base + 'api/v1/pictures/' + o.index, true);
			rand_call.send(null);
		}
		rand_call.onload = function(){
			var info = JSON.parse(rand_call.response);
			o.test.detail.str = info;
			o.leftbutton.dispatchEvent(o.test);
		}
	}
	goRight(o,elem, flyIn=false){
		var check = parseInt(o.toppic.style.left.slice(0, o.toppic.style.left.length-2))
		if(!flyIn){
			if(!flyIn && check >  o.refwin.clientWidth * 2) {
				o.state.toLoad = 'right';
				clearInterval(o.state.interval);
				o.leftbutton.dispatchEvent(o.test);
			}
		}else{
			if(check > o.refwin.clientWidth/2 - o.toppic.width/2){
				o.state.toLoad = 'done';
				o.test.detail.str = null;
				clearInterval(o.state.interval);
				o.leftbutton.dispatchEvent(o.test);
			}
		}
		o.toppic.style.left =  check + o.refwin.clientWidth * 0.05 + "px";
	}
	goLeft(o,elem, flyIn=false){
		var check = parseInt(o.toppic.style.left.slice(0, o.toppic.style.left.length-2))
		if(!flyIn){
			if(!flyIn && check <  o.refwin.clientWidth * -2) {
				o.state.toLoad = 'left';
				clearInterval(o.state.interval);
				o.leftbutton.dispatchEvent(o.test);
			}
		}else{
			if(check < o.refwin.clientWidth/2.5){
				console.log('peace out my dude');
				clearInterval(o.state.interval);
				o.state.toLoad = 'done';
				o.test.detail.str = null;
				o.leftbutton.dispatchEvent(o.test);
			}
		}
		o.toppic.style.left =  check - o.refwin.clientWidth * 0.05 + "px";
	}
}
var h;
function fadeClassIn(next_step, fadeThis){
	var e;
	for(var x = 0; x < $(fadeThis).length+1; x++){
		e = fadeThis + ':nth-child('+ x +')';
		$(e).animate({
			opacity: next_step
		},{
			duration:100,
		});
	}
}
function readjustCanvasButton(){
	var readjust = document.getElementById('navStyleCanvas');
	readjust.style.top = (window.innerHeight - readjust.clientHeight - 5) + "px";
}

$('.panel').css('opacity', 0);
var t = 100, step = 0.25;
for(var y = 0; step <= 1; y++){
	setTimeout(fadeClassIn.bind(null, step, '.panel'), t);
	t+= y;
	step += 0.25;
}
var tout;
$('#hoverExpand').hover(function(){
	if(!$('#nav-dropdown').is(':visible')) $('#nav-dropdown').slideDown('slow');
});
$('#hoverExpand').click(function(e){
	console.log(e);
	e.stopPropagation();
	e.preventDefault();
});
$('#hoverExpand').mouseleave(function(e){
	tout = setTimeout(function(){
		$('#nav-dropdown').slideUp('slow');
	}, 250);
});
$('#nav-dropdown').mouseenter(function(){
	clearTimeout(tout);
});
$('#nav-dropdown').mouseleave(function(){
	$('#nav-dropdown').slideUp('slow');
});
window.onload = function(){
	h = new Home(window);
	h.assignListeners(h);
	readjustCanvasButton();
}
window.onresize = function(){
	readjustCanvasButton();
	h.reAdjust();
}