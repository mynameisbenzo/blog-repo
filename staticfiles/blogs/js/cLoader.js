var speed = 1;

function addInput(w){
	w.addEventListener('keydown', function(e){
		switch(e.key){
			case 'ArrowDown':
				if(speed > 0.0625){
					speed /= 2;
				}
				break;
			case 'ArrowUp':
				if(speed < 0.75){
					speed *= 2;
				}
				break;
			case 'ArrowRight':
				break;
			case 'ArrowLeft':
				break;
			default:
				break;
		}
		console.log(speed);
	});
}
	//https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	function visibleHeightAtZDepth( depth, camera ) {
	  // compensate for cameras not positioned at z=0
	  const cameraOffset = camera.position.z;
	  if ( depth < cameraOffset ) depth -= cameraOffset;
	  else depth += cameraOffset;

	  // vertical fov in radians
	  const vFOV = camera.fov * Math.PI / 180; 

	  // Math.abs to ensure the result is always positive
	  return 2 * Math.tan( vFOV / 2 ) * Math.abs( depth );
	};
	function visibleWidthAtZDepth( depth, camera ){
	  const height = visibleHeightAtZDepth( depth, camera );
	  return height * camera.aspect;
	};

	var state = {
		'xDone': false,
		'yDone': false,
	}
	var bounds = 400;

		var scene = new THREE.Scene();
		var camera = new THREE.PerspectiveCamera(
			45,
			window.innerWidth / window.innerHeight,
			1,
			500);
			
		camera.position.set(0,0,100);
		camera.lookAt(new THREE.Vector3(0,0,0));
			
		var renderer = new THREE.WebGLRenderer();
		renderer.setSize(window.innerWidth, window.innerHeight);
		document.body.appendChild(renderer.domElement);
		
		// how to create a cube
		/*var geometry = new THREE.BoxGeometry(1,1,1);
		var material = new THREE.MeshBasicMaterial({color: 0x00ff00});
		var cube = new THREE.Mesh(geometry, material);
		scene.add(cube);*/

	function toNewLine(line){
		line.to_y = getRandomInt(-bounds, bounds);
		line.to_x = getRandomInt(-bounds, bounds);
		line.currentColor = 1;
		line.geometry.vertices[0].set(0,0,0);
		line.geometry.vertices[1].set(0,0,0);
	}
	function makeLine(){
		var lineColor = new THREE.Color(1,1,1);
		var material = new THREE.LineBasicMaterial({color:lineColor});
		
		var geometry = new THREE.Geometry();
		geometry.vertices.push(new THREE.Vector3(0,0,0));
		geometry.vertices.push(new THREE.Vector3(0,0,0));
		var line = new THREE.Line(geometry, material);
		scene.add(line);
		
		
		line.to_y = getRandomInt(-bounds, bounds);
		line.to_x = getRandomInt(-bounds, bounds);
		line.x1 = line.to_x/60, line.x2 = line.to_x/55;
		line.y1 = line.to_y/60, line.y2 = line.to_y/55;
		line.currentColor = 1;
		
		toNewLine(line);
		
		return line;
	}
		
	var lines = [];
	for(var i = 0; i < 200; i++){
		lines[i] = makeLine();
	}

	var toWhite = 0.01;

	function updateXY(line){
		if(line.to_x <= 0){
			line.geometry.vertices[1].x -= line.x1 * speed;
			line.geometry.vertices[0].x -= line.x2 * speed;
		}else{
			line.geometry.vertices[1].x += line.x1 * speed;
			line.geometry.vertices[0].x += line.x2 * speed;
		}
		if(line.to_y <= 0){
			line.geometry.vertices[0].y -= line.y2 * speed;
			line.geometry.vertices[1].y -= line.y1 * speed;
		}else{
			line.geometry.vertices[0].y += line.y2 * speed;
			line.geometry.vertices[1].y += line.y1 * speed;
		}
		if(Math.abs(line.geometry.vertices[0].x) > Math.abs(line.to_x)) toNewLine(line);
	}
	function updateColor(line){
		line.currentColor -= toWhite;
		line.material.color = new THREE.Color(line.currentColor,line.currentColor,line.currentColor);
	}
	function updateLine(){
		for(var i = 0; i < lines.length ; i++){
			updateColor(lines[i]);
			updateXY(lines[i]);
			lines[i].geometry.verticesNeedUpdate = true;
		}
	}
		
	window.addEventListener('resize', function() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize(window.innerWidth, window.innerHeight);
	});
		
	function animate(){
		updateLine();
		requestAnimationFrame(animate);
		renderer.render(scene, camera);
	}
	animate();