class GameObject{
	constructor(g){
	}
}
class Player{
	constructor(g){
	}
}
class Game{
	constructor(w){
		this.multiplier = 0.1;
		this.state = {
		};
		this.resetPos = {	
			x:-35,
			y:-35,
			z:0,
		};

		
		this.gameOver = document.getElementById('dead');
		this.soundBtn = document.getElementById('pauseplay');
		// Game.uiAdjust(this.soundBtn);
		
		this.scene = new THREE.Scene();
		this.camera = new THREE.PerspectiveCamera(
			45,
			window.innerWidth / window.innerHeight,
			1,
			500);
			
			
		this.camera.position.set(0,2,-10);
		this.camera.lookAt(new THREE.Vector3(0,0,0));
		
		
		this.renderer = new THREE.WebGLRenderer();
		this.renderer.setSize(window.innerWidth, window.innerHeight);
		this.renderer.domElement.id = 'getHeight';
		document.body.appendChild(this.renderer.domElement);
		
		// floor
		this.floorGeometry = new THREE.BoxGeometry(100, 1, 1);
		this.floorColor = new THREE.Color(1,1,1);
		this.floorMaterial = new THREE.MeshBasicMaterial({color:this.floorColor});
		this.floor = new THREE.Mesh(this.floorGeometry, this.floorMaterial);
		this.floor.position.set(this.resetPos.x, this.resetPos.y - 1, this.resetPos.z -1);
		this.scene.add(this.floor);
		
		this.doggo = null;
		
		this.setListeners(this,w);
	}
	loadObj(obj, type){
		var loader = new THREE.GLTFLoader();
		loader.setDRACOLoader(new THREE.DRACOLoader());
		var o = this;
		switch(type){
			case 'doggo':
				loader.load(obj,function(object){
					// object.scene.travers(function(c){
						// if(c.isMesh){
							// c.material.envMap = envMap;
						// }
					// });
					console.log(object.scene.children[0].position);
					console.log(object.scene.children[0]);
					o.scene.add(object.scene.children[0]);
					},function(xhr){
						console.log((xhr.loaded/xhr.total * 100) + "% loaded");
					},function(error){
						console.log('could not load');
					}
				);
				break;
			default:
				break;
		}
	}
	placeDoggo(){
		console.log(this.doggo);
		this.doggo.scale.setScalar(100);
		this.scene.add(this.doggo);
		this.camera.lookAt(this.doggo.position);
	}
	playSound(type,isBG=false){
		if(this.state.sounds.isMuted) return;
		var s = new THREE.Audio(this.listener);
		var audioLoader = new THREE.AudioLoader();
		audioLoader.load(this.state.sounds[type].src, function(buffer){
			s.setBuffer(buffer);
			s.setLoop(false);
			s.setVolume(0.5);
			s.play();
		});
	}
	updateUI(){
		if(this.scoreDisplay == undefined){
			this.scoreDisplay = document.getElementById('scoreDisplay');
			// Game.uiAdjust(this.scoreDisplay);
		}
		this.scoreDisplay.innerHTML = "Score:<br>" + this.state.score;
		switch(this.state.playing){
			case 0:
				(Game.mobileCheck()) ? this.gameOver.innerHTML = "Touch here to play!" :
					this.gameOver.innerHTML = "Click here to play!";
				// Game.uiAdjust(this.gameOver);
				break;
			case 1:
				this.gameOver.innerHTML = "";
				break;
			case 2:
				(Game.mobileCheck()) ? this.gameOver.innerHTML = this.state.GOmsg + "<br>Touch here to start again." :
										this.gameOver.innerHTML = this.state.GOmsg + "<br>Press Q to start again.";
				// Game.uiAdjust(this.gameOver);
				break;
			default:
				break;
		}
	}
	setListeners(o,w){
		window.addEventListener('resize', function() {
		});
		window.addEventListener('visibilitychange', function(){
		});
		window.addEventListener('mozvisibilitychange', function(){
		});
		window.addEventListener('webkitvisibilitychange', function(){
		});
		window.addEventListener('msvisibilitychange', function(){
		});
		window.addEventListener('blur', function(){ 
		});
		// document.getElementById('pauseplay').addEventListener('click',function(e){
		// });
	}
	createNewObject(){
		if(this.state.score > this.state.makeObj){
			this.state.objects.push(new GameObject(this));
			this.state.makeObj = this.state.score + 10;
		}
	}
	floorCheck(obj){
		// not on floor
		if(obj.origin.x < this.resetPos.x - this.state.floor.size/2 ||
			obj.origin.x > this.resetPos.x + this.state.floor.size/2) return true;
		// on floor
		return false;
	}
	resetGame(){
	}
	// toggle sounds 
	toggleSound(){
		if (this.state.sounds.bg.obj == null) return 
		(this.state.sounds.isMuted) ? this.state.sounds.bg.obj.setVolume(0) :
			this.state.sounds.bg.obj.setVolume(0.75);
	}
	 /*************************************************
		Static functions
	 **************************************************/
	 //https://discourse.threejs.org/t/functions-to-calculate-the-visible-width-height-at-a-given-z-depth-from-a-perspective-camera/269
	 static visibleWidthAtZDepth( depth, camera ){
		const height = Game.visibleHeightAtZDepth( depth, camera );
		return height * camera.aspect;
	}
	static visibleHeightAtZDepth( depth, camera ) {
		// compensate for cameras not positioned at z=0
		const cameraOffset = camera.position.z;
		if ( depth < cameraOffset ) depth -= cameraOffset;
		else depth += cameraOffset;

		// vertical fov in radians
		const vFOV = camera.fov * Math.PI / 180; 

		// Math.abs to ensure the result is always positive
		return 2 * Math.tan( vFOV / 2 ) * Math.abs( depth );
	}
	
	//https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
	static getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	static normalize(value, start, end){
		return (value - start)/(end-start);
	}
	// static uiAdjust(elem){
		// switch(elem.id){
			// case 'scoreDisplay':
				// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					// elem.style.left = "5%";
				// }
				// break;
			// case 'dead':
				// if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
					// elem.style.fontSize = "300%";
				// }
				// elem.style.left = document.body.clientWidth/2 - elem.clientWidth/2 + "px";
				// elem.style.top = "15%";
				// break;
			// case 'pauseplay':
				// elem.style.top = window.innerHeight - elem.clientHeight* 2 - 20 +"px";
				// elem.style.left = document.body.clientWidth/2 - elem.clientWidth/2 + "px";
				// break;
			// default:
				// break;
		// }
	// }
	static mobileCheck(){
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) return true;
		return false;
	}
	//main loop
	animate(){
		// if(this.doggo)this.floor.position = this.doggo.position;
		if(this.doggo){
			this.floor.position.x = this.doggo.position.x;
			this.floor.position.y = this.doggo.position.y;
			this.floor.position.z = this.doggo.position.z + 100;
			this.camera.lookAt(this.doggo.position);
		}
		// if(this.doggoparts){
			// for(var i = 0; i < this.doggoparts.length; i++){
				// this.doggoparts[i].position.setScalar(Game.getRandomInt(-100,100));
			// }
		// }
		requestAnimationFrame(this.animate.bind(this));
		this.renderer.render(this.scene, this.camera);
	}
}
var g = new Game(window);
g.animate();