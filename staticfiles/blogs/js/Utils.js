/******************************************************************************
		This class will contain all js static function that can have uses outside
		of it's own class.  All static functions.
*******************************************************************************/

class UTILS{ 
//https://discourse.threejs.org/t/functions-to-calculate-the-visible-width-height-at-a-given-z-depth-from-a-perspective-camera/269	
	static visibleWidthAtZDepth( depth, camera ){
		const height = UTILS.visibleHeightAtZDepth( depth, camera );
		return height * camera.aspect;
	}
	static visibleHeightAtZDepth( depth, camera ) {
		// compensate for cameras not positioned at z=0
		const cameraOffset = camera.position.z;
		if ( depth < cameraOffset ) depth -= cameraOffset;
		else depth += cameraOffset;

		// vertical fov in radians
		const vFOV = camera.fov * Math.PI / 180; 

		// Math.abs to ensure the result is always positive
		return 2 * Math.tan( vFOV / 2 ) * Math.abs( depth );
	}
	//https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
	static getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	static toRadians(angle) {
		return angle * (Math.PI / 180);
	}
	static mobileCheck(){
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) return true;
		return false;
	}
	//https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
	static validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
}