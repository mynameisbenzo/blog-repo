var needsReloading = [];

//https://stackoverflow.com/questions/29315787/draw-to-canvas-only-after-google-web-font-is-loaded
WebFontConfig = {
	google:{ families: ['VT323',] },
	active:function(){reload(needsReloading)},
};
(function(){
	var wf = document.createElement("script");
	wf.id = 'findThisScript';
	wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js';
	wf.async = 'true';
	document.head.appendChild(wf);
})();
function reload(needsReloading){
	for(var i = 0; i < needsReloading.length; i++){
		if(!(needsReloading[i] instanceof TextInput)) continue;
		needsReloading[i].loadText();
	}
	needsReloading.length = 0;
}
//https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
class EventBus{
	constructor(){
		// will store id and canvas object
		this.elements = {};
	}
	findTarget(name, element){
		var target = null;
		return target;
	}
	sendEvent(action){
	}
	//pass in canvas to put into eventbus
	addElement(element){
		this.elements[element.id] = element
	}
}
class Canvas{
	constructor(element){
		this.canvas = element;
		this.ctx = this.canvas.getContext('2d');
		this.ctx.font = "10px 'VT323'";
		this.canvas.style.backgroundColor = 'rgb(0, 0, 0)';
		this.x = 0, this.y = 0;
		this.setMiddle();

		// toggle states
		this.states = {
			"writing":false,
			"kbVisible":false,
		}
	}
	/*
		toggle left/right shift
	*/
	toggle(left=false){
		(this.states.kbVisible) ? this.shiftright(left) : this.shiftleft(left);
		this.states.kbVisible = !this.states.kbVisible;
	}
	/*
		defines middle of canvas object
	*/
	setMiddle(){
		this.centerx = this.canvas.width/2, this.centery = this.canvas.height/2;
	}
	// various ways to shift elements with its
	// preset values
	shiftup(){
		if(this.upShift == undefined) return;
		this.canvas.style.top = this.upShift;
	}
	shiftdown(){
		if(this.downShift == undefined) return;
		this.canvas.style.top = this.downShift;
	}
	shiftleft(left){
		if(this.leftShift == undefined) return;
		(left) ? this.canvas.style.left = this.leftShift : this.canvas.style.width = this.leftShift;
	}
	shiftright(left){
		if(this.rightShift == undefined) return;
		(left) ? this.canvas.style.left = this.rightShift : this.canvas.style.width = this.rightShift;
	}
	/*
		used to process calls loaded from phrases api
		this should be overridden for child classes
		so that phrases are processed in the appropriate
		manner
	*/
	processPhrase(phrase){
	}
	/*
		static methods
	*/
	// centers text in horizontal center.
	// note: consider expanding to vertical center.
	static createCenteringX(b){
		for(var i =0; i < 2; i++){
			b+=b;
		}
		return b;
	}
	// takes a given string message and turns it into pieces that fit within the
	// canvas object's width
	static createPieces(obj,str){
		var a_letter = /[a-z]/;
		var pieces = [];
		var i = 0, piece = "";
		while(true){
			if(str.length * (obj.canvas.clientWidth * 0.061 * obj.t_multiplier) > obj.canvas.clientWidth){
				for(var cs = 0; cs < str.length; cs++){
					if(cs * (obj.canvas.clientWidth * 0.061 * obj.t_multiplier) > obj.canvas.clientWidth) break;
					i = cs;
				}
				piece = str.slice(0, i);
				str = str.slice(i, str.length+1);
				if(a_letter.test(str[0].toLowerCase()) && piece[piece.length-1] != " ") piece += '-';
				pieces.push(piece);
			}else{
				pieces.push(str);
				break;
			}
		}
		return pieces;
	}
	//create chunk with given character
	static createChunk(obj, c, mod=0.061){
		var chunk = '';
		while((chunk.length * obj.canvas.clientWidth * mod * obj.t_multiplier) < obj.canvas.clientWidth){
			chunk += c;
		}
		return chunk;
	}
	//create chunk after being given a string
	static createChunkWithPrepend(obj, prep, mod=0.061){
		var chunk = prep;
		while((chunk.length * obj.canvas.clientWidth * mod * obj.t_multiplier) < obj.canvas.clientWidth){
			chunk += " ";
		}
		return chunk;
	}
	// check collision
	static checkCollision(ele1, ele2){
		if(ele1.x < ele2.x + ele2.br*2 &&
			ele1.x + ele1.br*2 > ele2.x &&
			ele1.y < ele2.y + ele2.br*2 &&
			ele1.y + ele1.br*2 > ele2.y){
			return true;
		}
		return false;
	}
	// get random phrase from json
	static getPhrase(o, type){
		var url = window.location;
		var base = url.protocol + "//" + url.host + "/" + url.pathname.split('/')[0];
		var xobj = new XMLHttpRequest();
		xobj.overrideMimeType("application/json");
		xobj.open('GET', base + 'api/v1/phrases/?type=' + type, true);
		xobj.send(null);
		xobj.onload  = function(){
			var info = JSON.parse(xobj.response);
			var pick = info['objects'];
			o.processPhrase(pick[getRandomInt(0, pick.length-1)]);
		}
	}
}
