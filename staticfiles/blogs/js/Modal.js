// this class has jQuery functions in it
// ...i'm sorry.
// I just wanted to work with the modal component easier!
// for the time let's just be one less lonely girl and handle one direction

/// the class takes in the actual modal itself or creates a modal
/// to create a modal there must be a mContainer class present
class Modal{
	constructor(elem){
		this.state = {
			// 0 no dir, 1 left->right, 2 right->left, other dirs will be 3-->
			t: 0,
			// modal visibility (onscreen/offscreen)
			visMain: false,
			// body visibility (fadein/fadeout) 0 vis, 1 invis load next, 2 invis load prev
			visBod: 0,
			movefrom:null,
			moveby: 50,
			expProjHold:{
				type:'',
				objects:null,
			}

		};
		this.emailSocket = null;
		(typeof(elem) == 'string') ? Modal.createModal(elem) : this.elem = elem;
		if(!this.elem)this.elem = $('#' +elem);
		this.init();

	}
	// position modal accordingly
	init(){
		(UTILS.mobileCheck()) ? this.elem.css('left','-150%') : this.elem.css('left','-100%');
		this.elem.css('top', $('.navbar').innerHeight() + 10 + "px");
		$('.modal-header').css('border-radius', '4px');
		this.elem.modal('show');
	}
	// used mainly in contact
	// keep socket alive
	keepAlive(){
		if(this.emailSocket.readyState == 3){
			var re = /localhost/;
			if(!re.test(window.location.href)){
				this.emailSocket = new WebSocket(
					'wss://' + window.location.host +
					'/wss/');
			}else{
				this.emailSocket = new WebSocket(
					'ws://' + window.location.host +
					'/ws/');
			}
		}
		setTimeout(this.keepAlive.bind(this),30000);
	}
	// send email with given content
	sendEmail(e, n, m){
		var email = e.split('@');
		var re = /localhost/;
		if(this.emailSocket == null){
			if(!re.test(window.location.href)){
				this.emailSocket = new WebSocket(
					'wss://' + window.location.host +
					'/wss/');
			}else{
				this.emailSocket = new WebSocket(
					'ws://' + window.location.host +
					'/ws/');
			}
			var o = this;
			this.emailSocket.onopen = function(e){
				o.emailSocket.send(
					JSON.stringify({
						'send_email':{
							'email': email,
							'name': n,
							'msg': m,
						}
					})
				);
				setTimeout(o.keepAlive(),30000);
			};
			this.emailSocket.onmessage = function(e){
			};
		}else{
			this.emailSocket.send(
				JSON.stringify({
					'send_email':{
						'email': email,
						'name': n,
						'msg': m,
					}
				})
			);
		}
	}
	setContent(c){
		switch(c){
			case 'about':
				this.buildAbout();
				break;
			case 'contact':
				this.buildContact();
				break;
			case 'experience':
				this.buildExp();
				break;
			case 'projects':
				this.buildProj();
				break;
			default:
				break;
		}
	}
	// build different types of modal displays
	buildAbout(){
		$('#modalMsg').html('About Me<button type="button" class="close" id="mDismiss" data-dismiss="modal">' +
			'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>');
		var me = ("My name is Lorenzo Hernandez III and I am a full stack developer.  More specifically, making sure " +
			"a website functions they way I want it to on both mobile and desktop.  I've also done some mobile development in the past " +
			"having worked on iOS and Android projects which you can find in the <i><b class='blackTextOverride'>My Work</b></i> section.  College courses exposed me to some " +
			"game development as well, which has been making Three.JS a blast to get into.  Rotate the cube to contact me if you have " +
			"any questions.");
		this.elem.css('overflow-y', 'hidden');
		var center = 'margin: 0 auto;'
		var style = "height:5%;"+ center +"text-align:center;font-size:150%;padding:10px;";
		$('.modal-body').html('<img class="img-responsive" style = "'+center+'" src="https://i.imgur.com/0vcD0I4.jpg"/><div style="'+ style +'" class="blackTextOverride" id="about">' + me +'</div>');
		$('#user-action').html("Dismiss");
		$('#user-action').css('color', 'white !important');
		$('#user-action').click(function(e){
			$('#mDismiss').click();
		});
		var o = this;
		$('#mDismiss').click(function(e){
			o.goDirection('left');
			e.preventDefault();
			e.stopPropagation();
		});
	}
	buildContact(){
		$('#modalMsg').html('Contact<button type="button" class="close" id="mDismiss" data-dismiss="modal">' +
			'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>');
		var m = ('<div class="form-group blackTextOverride"><label for="email">Email address:</label>' +
			'<input type="text" class="form-control" id="email" placeholder="Email"></div>' +
			'<div class="form-group blackTextOverride"><label for="name">Your name:</label>' +
			'<input type="text" class="form-control" id="name" placeholder="First Name, Last Name"></div>' +
			'<div class="blackTextOverride"><label for="message">Message:</label>' +
			'<textarea class="form-control" id="message" rows="8" placeholder="Message"></textarea></div>')
		$('.modal-body').html(m);
		$('#user-action').html("Send");
		$('#user-action').css('color', 'white !important');
		var o = this;
		$('#user-action').click(function(e){
			if(!UTILS.validateEmail($('#email').val()) ||
				$('#name').val().trim().length == 0 ||
				$('#message').val().trim().length == 0) {
				$('#email').val('');
				$('#name').val('');
				$('#message').val('');
				return;
			}else{
				o.sendEmail($('#email').val(),$('#name').val(),$('#message').val());
				$('#email').val('');
				$('#name').val('');
				$('#message').val('');
				$('#mDismiss').click();
			}
		});
		$('#mDismiss').click(function(e){
			o.goDirection('left');
			e.preventDefault();
			e.stopPropagation();
		});
	}
	buildProj(){
		if(this.state.expProjHold.type != "projects"){
			this.getData('p');
			return;
		}
		$('.modal-footer').html('');
		$('.modal-footer').html("<div class='btn-group' role='group'><button class='btn btn-primary' id='prev'>Previous</button>" +
			"<button class='btn btn-primary' id='next'>Next</button></div></div></div></div>");
		var head;
		(this.state.expProjHold.objects[0].img) ?
			head = ('<img class="mCon pull-left" src="'+this.state.expProjHold.objects[0].img+'""/>' +
				this.state.expProjHold.objects[0].title +
				'<button type="button" class="close" id="mDismiss" data-dismiss="modal">' +
				'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>') :
			head = (this.state.expProjHold.objects[0].title +
					'<button type="button" class="close" id="mDismiss" data-dismiss="modal">' +
					'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>');
		$('#modalMsg').html(head);
		var style = "height:5%;margin: 0 auto;font-size:150%;padding:10px;";
		var mList = this.state.expProjHold.objects[0].port_desc;
		var me = '<div style="'+ style +'" class="blackTextOverride" id="about">';
		if(this.state.expProjHold.objects[0].link) {
			me += ('<a href="'+this.state.expProjHold.objects[0].link+'"><div class=' +
				'"alert alert-success aMod" role="alert">Relevant Link</div></a>');
		}
		me += this.state.expProjHold.objects[0].port_desc + "</div>";
		this.elem.css('overflow-y', 'hidden');
		$('.modal-body').html(me);
		var o = this;
		$('#next').click(function(e){
			if(o.state.visBod == 0){
				o.state.visBod = 1;
			}
		});
		$('#prev').click(function(e){
			if(o.state.visBod == 0){
				o.state.visBod = 2;
			}
		});
		$('#mDismiss').click(function(e){
			o.goDirection('left');
			e.preventDefault();
			e.stopPropagation();
		});
	}
	buildExp(){
		if(this.state.expProjHold.type != "experience"){
			this.getData('e');
			return;
		}
		$('.modal-footer').html("<div class='btn-group' role='group'><button class='btn btn-primary' id='prev'>Previous</button>" +
			"<button class='btn btn-primary' id='next'>Next</button></div></div></div></div>");
		$('#modalMsg').html(this.state.expProjHold.objects[0].title +
			'<button type="button" class="close" id="mDismiss" data-dismiss="modal">' +
			'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>');
		var style = "height:5%;margin: 0 auto;font-size:150%;padding:10px;";
		var mList = this.state.expProjHold.objects[0].port_desc;
		var sList = mList.split('\n');
		var me = '<div style="'+ style +'" class="blackTextOverride" id="about"><ul>';
		for(var i = 0; i < sList.length; i++){
			if(sList[i].length < 2) continue;
			me += '<li>'+sList[i]+'</li>';
		}
		me += "</ul>";
		this.elem.css('overflow-y', 'hidden');
		if(this.state.expProjHold.objects[0].link) {
			me += ('<a href="'+this.state.expProjHold.objects[0].link+'"><div class=' +
				'"alert alert-success" role="alert">Relevant Link</div></a></div>');
		}else{
			me += "</div>";
		}
		$('.modal-body').html(me);
		var o = this;
		$('#next').click(function(e){
			if(o.state.visBod == 0){
				o.state.visBod = 1;
			}
		});
		$('#prev').click(function(e){
			if(o.state.visBod == 0){
				o.state.visBod = 2;
			}
		});
		$('#mDismiss').click(function(e){
			o.goDirection('left');
			e.preventDefault();
			e.stopPropagation();
		});
	}
	// the following are used in modal actions
	// determine direction
	goDirection(dir){
		if(this.checkDone(dir)) return;
		switch(dir){
			case 'right':
				this.state.t = 1;
				break;
			case 'left':
				this.state.t = 2;
				break;
			case 'up':
				this.state.t = 4;
				break;
			case 'middle':
				this.state.t = 3;
				break;
			default:
				break;
		}
	}
	// check if transition is done
	checkDone(dir){
		switch(dir){
			case 'left':
				if(parseInt(this.elem.css('left')) <= document.body.clientWidth * -1) return true;
				return false;
				break;
			case 'right':
				if(parseInt(this.elem.css('left')) >= document.body.clientWidth/10) return true;
				return false;
				break;
			default:
				break;
		}
	}
	// move modal to the right middle
	goRight(){
		if(this.state.movefrom == null) {
			this.state.movefrom = parseInt(this.elem.css('left'));
			if(UTILS.mobileCheck()) this.elem.css('right', document.body.clientWidth + 'px');
		}else if(this.checkDone('right')) {
			this.elem.css('left', document.body.clientWidth/10 + 'px');
			this.elem.css('right', document.body.clientWidth/10 + 'px');
			this.state.t = 0;
			this.state.visMain = true;
			return;
		}
		this.state.movefrom += this.state.moveby;
		this.elem.css('left', this.state.movefrom + 'px');
		if(parseInt(this.elem.css('right')) > document.body.clientWidth/10) {
			this.elem.css('right', this.state.movefrom * -1 +"px");
		}
	}
	// move modal offscreen
	goLeft(){
		if(this.state.movefrom == null) {
			this.state.movefrom = parseInt(this.elem.css('left'));
		}else if(this.checkDone('left')) {
			this.elem.css('left', document.body.clientWidth * -2 + 'px');
			this.elem.css('right', document.body.clientWidth  + 'px');
			this.state.t = 0;
			this.state.visMain = false;
			return;
		}
		this.state.movefrom -= this.state.moveby;
		this.elem.css('left', this.state.movefrom + 'px');
		if(parseInt(this.elem.css('right')) < document.body.clientWidth) {
			this.elem.css('right', this.state.movefrom * -1 +"px");
		}
	}
	// following are used mainly in projects/experience switching
	// get project/experience data
	getData(type){
		var o = this;
		var url = window.location;
		var base = url.protocol + "//" + url.host + "/" + url.pathname.split('/')[0];
		var xobj = new XMLHttpRequest();
		xobj.overrideMimeType("application/json");
		switch(type){
			case 'p':
				xobj.open('GET', base + 'api/v1/projects/', true);
				xobj.send(null);
				xobj.onload  = function(){
					var info = JSON.parse(xobj.response);
					var pick = info['objects'];
					o.state.expProjHold.type = 'projects';
					o.state.expProjHold.objects = pick;
					o.buildProj();
				}
				break;
			case 'e':
				xobj.open('GET', base + 'api/v1/experience/', true);
				xobj.send(null);
				xobj.onload  = function(){
					var info = JSON.parse(xobj.response);
					var pick = info['objects'];
					o.state.expProjHold.type = 'experience';
					o.state.expProjHold.objects = pick;
					o.buildExp();
				}
				break;
			default:
				break;
		}
	}
	// transition while switching to next object
	// reload with next object if done fading out
	fadeIn(){
		if(parseFloat($('.modal-body').css('opacity')) >= 1) return;
		$('.modal-body').css('opacity', parseFloat($('.modal-body').css('opacity')) + 0.05);
	}
	fadeOut(){
		if(parseFloat($('.modal-body').css('opacity')) <= 0){
			(this.state.visBod == 1) ? this.rotateObjects() : this.rotateObjects(false);
			this.state.visBod = 0;
			(this.state.expProjHold.type == 'experience') ? this.buildExp(): this.buildProj();
			return;
		}
		$('.modal-body').css('opacity', parseFloat($('.modal-body').css('opacity')) - 0.05);
	}
	// true = right, false = left
	rotateObjects(dir=true){
		if(dir){
			var f = this.state.expProjHold.objects[0];
			this.state.expProjHold.objects.shift();
			this.state.expProjHold.objects.push(f);
		}else{
			var f = this.state.expProjHold.objects.pop();
			this.state.expProjHold.objects.unshift(f);
		}

	}
	// static functions
	static createModal(id){
		if(!document.getElementById('mContainer')) return;
		document.getElementById('mContainer').innerHTML += ("<div class='modal fade' role='dialog' data-backdrop='false' id='"+id+"'>" +
			"<div class='modal-dialog' ><div class='modal-content'><div class='modal-header'><h4 class='modal-title text-center' id='modalMsg'>" +
			"</h4></div><div class='modal-body'></div><div class='modal-footer'></div>");
	}

	// update the necessary parts of the modal
	update(){
		switch(this.state.t){
			case 0:
				this.state.movefrom = null
				break;
			case 1:
				this.goRight();
				break;
			case 2:
				this.goLeft();
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
		}
		switch(this.state.visBod){
			case 0:
				this.fadeIn();
				break;
			case 1:
			case 2:
				this.fadeOut()
				break;
			default:
				break;
		}
	}
}
